# Sheldon

## Instrukcja obsługi
### Przed uruchomieniem
1. Instalacja kompilatora golang.
2. Instalacja Docker Desktop (można rozważyć późniejszą przesiadkę na Podmana - jeszcze nie przemyślałem tego)
3. Pobranie obrazu postgresa `docker pull postgres:alpine` 

### Jak uruchomić
1. Kontener z Postgresem. **WAŻNE: aby postgres był persistent w kontenerze (nie tracił zapisanych danych po
restarcie), trzeba mieć jakiś lokalnie utworzony katalog przeznaczony na dane postgresa, poniżej jest to
./rest/db/dbdata - w bashu `${PWD}/rest/db/dbata`**
```shell
docker run -d \
	--name sheldon-postgres \
    -p 5432:5432 \
	-e POSTGRES_USER=admin \
	-e POSTGRES_PASSWORD=mysecretpassword \
	-e POSTGRES_DB=sheldon \
	-e PGDATA=/var/lib/postgresql/data/pgdata \
	-v "${PWD}/rest/db/dbdata":/var/lib/postgresql/data \
	postgres:alpine
```
2. Uruchomienie API w Go
```shell
cd rest
go run cmd/sheldonAPI/main.go
```
3. No i strzelamy pod endpointy curlem/postmanem - co wolimy.

### Jak obsługiwać Postgresa
Jeśli chcemy wejść do bazy i pogrzebać, to można:
```shell
docker exec -it sheldon-postgres psql -d sheldon -U admin
```
Albo jeśli chcemy pogrzebać w samym kontenerze (jakieś pingi itd.), to
można odpalić sesję basha w kontenerze:
```shell
docker exec -it sheldon-postgres /bin/bash
```
Baza chodzi pod adresem `localhost:5432`.
Można więc też użyć jakiegoś frontendu do baz danych - ja polecam
DBeaver, ale pgAdmin też się wydaje bardzo fajny.

### Jak tworzyć/robić migracje
Raczej będziemy używać narzędzia [migrate](https://github.com/golang-migrate/migrate).

[Tutorial do postgresa](https://github.com/golang-migrate/migrate/blob/master/database/postgres/TUTORIAL.md).

W skrócie:
1. Tworzymy sobie nową migrację
```shell
cd rest;
migrate create -ext sql -dir db/migrations -seq NAZWA_MIGRACJI
```
Powstaną 2 pliki - 1 z koncówką "up" a drugi "down".
- w "up" wpisujemy jak wprowadzić zmianę do bazy np. `ALTER TABLE users ADD COLUMN is_cool BOOLEAN DEFAULT FALSE;`
- w "down" wpisujemy jak usunąć zmianę np. `ALTER TABLE users DROP COLUMN IF EXISTS is_cool;`

Robimy to zgodnie z zasadą idempotencji. No i fajnie też robić to w transakcjach SQLowych np.
```sql
BEGIN;
-- ALTER TABLE ... czary mary hokus pokus simsalabim
-- (baza rozwalona)
END;
```

2. Migrujemy bazę
```shell
POSTGRESQL_URL='postgres://admin:mysecretpassword@localhost:5432/sheldon?sslmode=disable'
migrate -database "${POSTGRESQL_URL}" -path db/migrations up
```

