import { useContext } from "react";
import AuthContext from "../context/AuthProvider.tsx";

const useAuth = () => {
  const { auth, setAuth, clearAuth } = useContext(AuthContext);

  const decodeToken = (token: string) => {
    try {
      const base64Url = token.split(".")[1];
      const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
      const jsonPayload = decodeURIComponent(
        atob(base64)
          .split("")
          .map(function (c) {
            return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
          })
          .join(""),
      );
      return JSON.parse(jsonPayload);
    } catch (error) {
      console.log("Invalid JWT Token", error);
      return null;
    }
  };

  const getRole = () => {
    if (!auth?.jwtAccessToken) {
      return null;
    }

    const decodedToken = decodeToken(auth.jwtAccessToken);
    return decodedToken?.role;
  };

  const getId = () => {
    if (!auth?.jwtAccessToken) {
      return null;
    }

    const decodedToken = decodeToken(auth.jwtAccessToken);
    return decodedToken?.id;
  };

  const isTokenExpired = () => {
    if (!auth?.jwtAccessToken) {
      return true;
    }

    const decodedToken = decodeToken(auth.jwtAccessToken);
    const expiry = decodedToken?.exp;

    if (!expiry) {
      return true;
    }

    return Date.now() >= expiry * 1000;
  };

  return { auth, setAuth, getRole, getId, isTokenExpired, clearAuth };
};

export default useAuth;
