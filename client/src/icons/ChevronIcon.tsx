
enum ChevronDirection {
  LEFT, RIGHT
}

interface ChevronProps {
  className: string;
  strokeWidth: number;
  direction: ChevronDirection,
}

const ChevronIcon = ({ strokeWidth, className, direction }: ChevronProps) => (
  <svg 
    xmlns="http://www.w3.org/2000/svg"
    fill="none" 
    viewBox="0 0 24 24"
    strokeWidth={strokeWidth}
    stroke="currentColor"
    className={className}>
    {getSvgPath(direction)}
  </svg>
);

const getSvgPath = (direction: ChevronDirection) => {
  switch (direction) {
  case ChevronDirection.LEFT:
    return (
      <path 
        stroke-linecap="round"
        stroke-linejoin="round"
        d="M15.75 19.5 8.25 12l7.5-7.5"
      />
    );
  case ChevronDirection.RIGHT:
    return (
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="m8.25 4.5 7.5 7.5-7.5 7.5"
      />
    );
  }
}

export {ChevronIcon, ChevronDirection};
