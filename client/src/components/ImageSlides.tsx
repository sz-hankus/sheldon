import { useState } from "react";
import {ChevronIcon, ChevronDirection} from "../icons/ChevronIcon";

export interface ImageSlidesProps {
  urls: string[];
}

const ImageSlides = ({ urls }: ImageSlidesProps) => {
  const [currentIndex, setCurrentIndex] = useState<number>(0);

  const next = () => {
    setCurrentIndex((currentIndex + 1) % urls.length) 
  }

  const prev = () => {
    setCurrentIndex((currentIndex - 1 + urls.length) % urls.length) 
  }

  return (
    <div className="relative min-w-96 max-w-sm bg-white border border-gray-200 rounded-lg shadow">
      <div className="absolute left-0 flex items-center h-full">
        <button onClick={prev} className=""> 
          <ChevronIcon strokeWidth={1.5} className="w-8 h-8 stroke-gray-300 hover:stroke-gray-600" direction={ChevronDirection.LEFT}/> 
        </button>
      </div>
      <div className="absolute right-0 flex items-center h-full">
        <button onClick={next} className=""> 
          <ChevronIcon strokeWidth={1.5} className="w-8 h-8 stroke-gray-300 hover:stroke-gray-600" direction={ChevronDirection.RIGHT}/> 
        </button>
      </div>
      <img
        className="rounded-t-lg w-96 h-56"
        src={urls[currentIndex]}
        alt=""
      />
    </div>
  );
};

export default ImageSlides;
