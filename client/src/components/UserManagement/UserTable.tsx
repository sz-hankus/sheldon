import { User } from "../../models/User";
import UserRow from "./UserRow";

interface UserTableProps {
  users: User[] | null;
  setUsers: React.Dispatch<React.SetStateAction<User[]>>;
}

const UserTable = ({ users, setUsers }: UserTableProps) => (
  <table className="w-full">
    <thead className="bg-gray-100">
      <tr>
        <th className="w-16 p-2 text-sm font-semibold text-left">ID</th>
        <th className="p-2 text-sm font-semibold text-left">Email</th>
        <th className="w-16 p-2 text-sm font-semibold text-center">Rola</th>
        <th className="p-2 text-sm font-semibold text-left">Data utworzenia</th>
        <th className="w-40 p-2 text-sm font-semibold text-left">Akcje</th>
      </tr>
    </thead>
    <tbody>
      {users?.map((user) => (
        <UserRow key={user.id} user={user} setUsers={setUsers} />
      ))}
    </tbody>
  </table>
);

export default UserTable;
