import { Link } from "react-router-dom";
import { User } from "../../models/User";
import Badge from "./Badge";
import DeleteModal from "./DeleteModal";
import EditModal from "./EditModal";

interface UserRowProps {
  user: User;
  setUsers: React.Dispatch<React.SetStateAction<User[]>>;
}

const UserRow = ({ user, setUsers }: UserRowProps) => {
  return (
    <tr key={user.id} className="border-b hover:bg-blue-50">
      <td className="p-3 text-sm font-medium text-left">{user.id}</td>
      <Link to={`/users/${user.id}`}>
        <td className="p-3 text-sm font-medium text-left">{user.email}</td>
      </Link>
      <td className="p-3 text-sm font-medium text-center">
        <Badge role={user.role} />
      </td>
      <td className="p-3 text-sm font-medium text-left">{user.creationTime}</td>
      <td className="p-3 flex text-sm font-medium text-left space-x-4">
        <EditModal user={user} setUsers={setUsers} />
        <DeleteModal user={user} setUsers={setUsers} />
      </td>
    </tr>
  );
};

export default UserRow;
