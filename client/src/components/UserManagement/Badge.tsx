interface BadgeProps {
  role: string;
}

const Badge = ({ role }: BadgeProps) => (
  <span
    className={`p-2 text-xs font-medium tracking-wider rounded-full bg-opacity-80 ${getBadgeColor(
      role
    )}`}
  >
    {role}
  </span>
);

const getBadgeColor = (role: string) => {
  switch (role) {
  case "super_admin":
    return "bg-red-200 text-red-900";
  case "admin":
    return "bg-blue-200 text-blue-900";
  case "lab_master":
    return "bg-yellow-200 text-yellow-900";
  case "reserver":
    return "bg-green-200 text-green-900";
  }
};

export default Badge;
