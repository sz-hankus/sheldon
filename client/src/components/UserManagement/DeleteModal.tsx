import { useState } from "react";
import Modal from "../Modal";
import { deleteUser } from "../../api/usersApiClient";
import { User } from "../../models/User";
import useAuth from "../../hooks/useAuth";
import TrashIcon from "../../icons/TrashIcon";

interface DeleteModalProps {
  user: User;
  setUsers: React.Dispatch<React.SetStateAction<User[]>>;
}

const DeleteModal = ({ user, setUsers }: DeleteModalProps) => {
  const [isDeleteOpen, setIsDeleteOpen] = useState<boolean>(false);

  const { auth } = useAuth();

  const handleDeleteButton = async () => {
    try {
      await deleteUser(user.id, auth?.jwtAccessToken ?? "");
      setUsers((prevUsers) => prevUsers.filter((u) => u.id !== user.id));
      setIsDeleteOpen(false);
    } catch (err) {
      console.log("error deleting user", err);
    }
  };

  return (
    <>
      <button
        onClick={() => setIsDeleteOpen(true)}
        className="flex text-red-600 hover:text-red-900 space-x-1"
      >
        <TrashIcon className="w-5 h-5" strokeWidth={1.5} />
        <span>Usuń</span>
      </button>

      <Modal isOpen={isDeleteOpen} onClose={() => setIsDeleteOpen(false)}>
        <h3 className="text-base text-center">
          Czy jesteś pewny, że chcesz usunąć użytkownika{" "}
          <span className="font-semibold">{user.email}</span>?
        </h3>
        <div className="text-center">
          <button
            onClick={handleDeleteButton}
            className="py-2.5 px-3.5 mt-4 font-medium text-white bg-red-600 rounded-md hover:bg-red-700"
          >
            Tak, usuń
          </button>
          <button
            onClick={() => setIsDeleteOpen(false)}
            className="py-2.5 px-3.5 mt-4 font-medium rounded-md border ml-5"
          >
            Nie, anuluj
          </button>
        </div>
      </Modal>
    </>
  );
};

export default DeleteModal;
