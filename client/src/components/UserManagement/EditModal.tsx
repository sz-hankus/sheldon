import { useState } from "react";
import { User } from "../../models/User";
import useAuth from "../../hooks/useAuth";
import { updateUser } from "../../api/usersApiClient";
import EditIcon from "../../icons/EditIcon";
import Modal from "../Modal";

interface EditModalProps {
  user: User;
  setUsers: React.Dispatch<React.SetStateAction<User[]>>;
}

const EditModal = ({ user, setUsers }: EditModalProps) => {
  const [isEditOpen, setIsEditOpen] = useState<boolean>(false);
  const [userDetails, setUserDetails] = useState<User>({
    id: user.id,
    email: user.email,
    role: user.role,
    creationTime: user.creationTime,
    imageRef: user.imageRef
  });

  const { auth } = useAuth();

  const handleEditButton = async () => {
    try {
      await updateUser(user.id, auth?.jwtAccessToken ?? "", userDetails);
      setUsers((prevUsers) =>
        prevUsers.map((u) => (u.id === user.id ? { ...u, ...userDetails } : u))
      );
      setIsEditOpen(false);
    } catch (err) {
      console.log("error updating user", err);
    }
  };

  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    const { name, value } = e.target;
    setUserDetails((prevDetails) => ({
      ...prevDetails,
      [name]: value,
    }));
  };

  return (
    <>
      <button
        onClick={() => setIsEditOpen(true)}
        className="flex text-blue-600 hover:text-blue-900 space-x-1"
      >
        <EditIcon className="w-5 h-5" strokeWidth={1.5} />
        <span>Edytuj</span>
      </button>

      <Modal isOpen={isEditOpen} onClose={() => setIsEditOpen(false)}>
        <h3 className="text-2xl font-bold md:w-80">Edytuj użytkownika</h3>
        <form>
          <div className="mt-4">
            <label htmlFor="id" className="block mb-2 font-medium">
              ID
            </label>
            <input
              type="number"
              id="id"
              name="id"
              value={userDetails.id}
              required
              disabled
              className="border border-gray-300 rounded-lg block p-2.5 w-full disabled:bg-gray-200"
            />
          </div>
          <div className="mt-4">
            <label
              htmlFor="registration-email"
              className="block mb-2 font-medium"
            >
              Email
            </label>
            <input
              type="email"
              id="registration-email"
              name="email"
              value={userDetails.email}
              onChange={handleChange}
              required
              className="border border-gray-300 rounded-lg block p-2.5 w-full"
            />
          </div>
          <div className="mt-4">
            <label htmlFor="created_at" className="block mb-2 font-medium">
              Data utworzenia
            </label>
            <input
              type="text"
              id="created_at"
              name="created_at"
              value={userDetails.creationTime}
              required
              disabled
              className="border border-gray-300 rounded-lg block p-2.5 w-full disabled:bg-gray-200"
            />
          </div>
          <div className="mt-4">
            <label htmlFor="role" className="block mb-2 font-medium">
              Rola
            </label>
            <select
              id="role"
              value={userDetails.role}
              name="role"
              onChange={handleChange}
              required
              className="bg-white border border-gray-300 rounded-lg block p-2.5 w-full"
            >
              <option value="reserver">Rezerwujący</option>
              <option value="lab_master">Technik</option>
              <option value="admin">Admin</option>
            </select>
          </div>
        </form>
        <div className="flex ">
          <button
            onClick={handleEditButton}
            className="py-2.5 w-full mt-4 font-medium text-white bg-blue-600 rounded-md hover:bg-blue-700"
          >
            Zapisz
          </button>
          <button
            onClick={() => setIsEditOpen(false)}
            className="py-2.5 px-3.5 mt-4 font-medium rounded-md border ml-5"
          >
            Anuluj
          </button>
        </div>
      </Modal>
    </>
  );
};

export default EditModal;
