import { Reservation } from "../models/Reservation.ts";
import { utcToZonedTime } from "date-fns-tz";
import { format } from "date-fns";
import { getUser } from "../api/usersApiClient.ts";
import useAuth from "../hooks/useAuth.ts";
import { useEffect, useState } from "react";

interface PopoverProps {
  isVisible: boolean;
  reservation: Reservation;
}

const Popover = ({ isVisible, reservation }: PopoverProps) => {
  const { auth } = useAuth();
  const [email, setEmail] = useState<string | null>(null);

  useEffect(() => {
    const fetchEmail = async () => {
      return await getUser(
        Number(reservation.reserverId),
        auth?.jwtAccessToken ?? "",
      );
    };
    fetchEmail().then((user) => setEmail(user.email));
  }, [auth?.jwtAccessToken, reservation]);

  return (
    <>
      {isVisible && (
        <div
          role="tooltip"
          className="w-2/3 left-1/2 transform -translate-x-3/4 fixed md:absolute bottom-5 z-10 md:w-fit text-sm bg-white text-black border rounded-lg shadow-2xl border-gray-500"
        >
          <div className="px-3 py-2 bg-blue-700 text-white rounded-t-lg">
            <h3 className="font-semibold">Rezerwacja</h3>
          </div>
          <div className="flex flex-col px-3 py-2 w-max">
            <span>Zarezerwowano przez: {email}</span>
            <span>
              Od:{" "}
              {format(utcToZonedTime(reservation.startTime, "CET"), "HH:mm")}
            </span>
            <span>
              Do: {format(utcToZonedTime(reservation.endTime, "CET"), "HH:mm")}
            </span>
          </div>
        </div>
      )}
    </>
  );
};

export default Popover;
