import { useEffect, useState } from "react";
import PlusIcon from "../icons/PlusIcon";
import { User } from "../models/User";
import useAuth from "../hooks/useAuth";
import { getUsers } from "../api/usersApiClient";
import UserTable from "./UserManagement/UserTable";
import SearchBox from "./SearchBox";

const Users = () => {
  const [users, setUsers] = useState<User[]>([]);

  const { auth } = useAuth();

  useEffect(() => {
    const fetchUser = async () => {
      return await getUsers(auth?.jwtAccessToken ?? "");
    };
    fetchUser().then((users) => setUsers(users));
  }, [auth?.jwtAccessToken]);


  // Update the list only after a period between keystrokes greater than 300ms occurs
  let timer = 0;
  const updateFilteredUsers = (email: string) => {
    clearTimeout(timer);
    timer = setTimeout(async () => {
      console.log(email)
      await getUsers(auth?.jwtAccessToken ?? "", email).then(users => setUsers(users))
    }, 300)
  }

  return (
    <section className="flex flex-col flex-grow px-6 py-3 bg-gray-50">
      <div className="flex h-10 w-full justify-between items-center">
        <div className="flex h-7">
          <h1 className="text-xl font-bold pr-2 border-r border-gray-300">
            Użytkownicy
          </h1>
          <span className="flex items-center pl-2 h-full text-gray-600">
            {users?.length ?? 0}
          </span>
        </div>
        <button className="text-white text-sm flex items-center bg-blue-700 hover:bg-blue-900 font-medium rounded-md text-center py-2.5 px-1.5">
          <PlusIcon className="w-4 h-4 mr-0.5" strokeWidth={1.5} />
          <span className="hidden min-[390px]:inline">Dodaj użytkownika</span>
        </button>
      </div>

      <SearchBox onChange={updateFilteredUsers} />

      <div className="mt-3 bg-white overflow-auto">
        <UserTable users={users} setUsers={setUsers} />
      </div>
    </section>
  );
};

export default Users;
