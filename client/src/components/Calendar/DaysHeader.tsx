import { format } from "date-fns";

interface DaysHeaderProps {
  daysInWeek: Date[];
}

const WEEKDAYS_SHORT = ["Pn", "Wt", "Śr", "Cz", "Pt", "So", "Nd"];

const DaysHeader = ({ daysInWeek }: DaysHeaderProps) => {
  return (
    <>
      <div></div>
      {daysInWeek.map((day, index) => (
        <div key={`header-${index}`} className="font-medium text-center pb-3">
          <div>{WEEKDAYS_SHORT[index]}</div>
          <div className="text-2xl font-semibold">{format(day, "d")}</div>
        </div>
      ))}
    </>
  );
};

export default DaysHeader;
