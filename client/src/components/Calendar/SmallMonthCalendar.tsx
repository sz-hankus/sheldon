import {
  addDays,
  eachDayOfInterval,
  endOfMonth,
  format,
  getISODay,
  startOfMonth,
  subDays,
} from "date-fns";
import { pl } from "date-fns/locale";

const WEEKDAYS_SHORT = ["Pn", "Wt", "Śr", "Cz", "Pt", "So", "Nd"];

const SmallMonthCalendar = () => {
  const currentDate = new Date();
  const firstDayOfMonth = startOfMonth(currentDate);
  const endDayOfMonth = endOfMonth(currentDate);
  const lastDayOfMonth = endOfMonth(currentDate);

  const daysInMonth = eachDayOfInterval({
    start: firstDayOfMonth,
    end: lastDayOfMonth,
  });

  const getLastDaysOfPreviousMonth = () => {
    const startingDayIndex = getISODay(firstDayOfMonth) - 1;
    if (startingDayIndex === 0) return [];

    return eachDayOfInterval({
      start: subDays(firstDayOfMonth, startingDayIndex),
      end: subDays(firstDayOfMonth, 1),
    });
  };

  const getFirstDaysOfNextMonth = () => {
    const endingDayIndex = getISODay(endDayOfMonth) - 1;
    if (endingDayIndex === 6) return [];

    return eachDayOfInterval({
      start: addDays(lastDayOfMonth, 1),
      end: addDays(lastDayOfMonth, 6 - endingDayIndex),
    });
  };

  return (
    <div className="flex flex-col justify-center mx-auto">
      <div className="text-center mb-3">
        <h3 className="font-medium">
          {format(currentDate, "LLLL yyyy", { locale: pl })}
        </h3>
      </div>
      <div className="grid grid-cols-7 gap-3">
        {WEEKDAYS_SHORT.map((day) => (
          <div key={day} className="text-center font-medium">
            {day}
          </div>
        ))}
        {getLastDaysOfPreviousMonth().map((day, index) => (
          <div key={`prev-${index}`} className="text-center text-gray-500">
            {format(day, "d")}
          </div>
        ))}
        {daysInMonth.map((day, index) => (
          <div key={index} className="text-center font-medium">
            {format(day, "d")}
          </div>
        ))}
        {getFirstDaysOfNextMonth().map((day, index) => (
          <div key={`next-${index}`} className="text-center text-gray-500">
            {format(day, "d")}
          </div>
        ))}
      </div>
    </div>
  );
};

export default SmallMonthCalendar;
