import React from "react";

interface HoursColumnProps {
  hour: number;
  children: React.ReactNode;
}
const HoursColumn = ({ hour, children }: HoursColumnProps) => (
  <React.Fragment>
    <div className="text-right text-gray-500">
      <span className="text-xs pr-1">
        {hour.toString().padStart(2, "0")}:00
      </span>
    </div>
    {children}
  </React.Fragment>
);

export default HoursColumn;
