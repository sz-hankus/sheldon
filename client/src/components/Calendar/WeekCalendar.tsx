import { eachDayOfInterval, endOfISOWeek, startOfISOWeek } from "date-fns";
import { useEffect } from "react";
import { useState } from "react";
import { Reservation } from "../../models/Reservation.ts";
import { getReservations } from "../../api/reservationApiClient.ts";
import useAuth from "../../hooks/useAuth.ts";
import DaysHeader from "./DaysHeader.tsx";
import HoursColumn from "./HoursColumn.tsx";
import DaysColumn from "./DaysColumn.tsx";

interface WeekCalendarProps {
  stationFilter?: number;
}

const WeekCalendar = ({ stationFilter }: WeekCalendarProps) => {
  const [reservations, setReservations] = useState<Reservation[]>([]);

  const currentDate = new Date();
  const daysInWeek = eachDayOfInterval({
    start: startOfISOWeek(currentDate),
    end: endOfISOWeek(currentDate),
  });

  const hoursRange = Array.from({ length: 15 }, (_, i) => i + 7);

  const { auth } = useAuth();

  useEffect(() => {
    const fetchReservations = async () => {
      return await getReservations(auth?.jwtAccessToken ?? "", stationFilter);
    };

    fetchReservations().then((reservations) => setReservations(reservations));
  }, [auth?.jwtAccessToken, stationFilter]);

  return (
    <div className="flex flex-col justify-center w-full px-8 md:pl-0 py-8 bg-gray-50">
      <div
        className="grid grid-cols-8 text-gray-600 pr-4"
        style={{ gridTemplateColumns: "auto repeat(7, 1fr)" }}
      >
        <DaysHeader daysInWeek={daysInWeek} />
        {hoursRange.map((hour) => (
          <HoursColumn hour={hour} key={`hour-${hour}`}>
            {daysInWeek.map((_, index) => (
              <DaysColumn
                key={`day-${index}`}
                reservations={reservations}
                index={index}
                hour={hour}
              />
            ))}
          </HoursColumn>
        ))}
      </div>
    </div>
  );
};

export default WeekCalendar;
