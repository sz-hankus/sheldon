import { utcToZonedTime } from "date-fns-tz";
import { format } from "date-fns";
import { Reservation } from "../../models/Reservation.ts";
import Popover from "../Popover.tsx";
import { useState } from "react";

interface DaysColumnProps {
  index: number;
  hour: number;
  reservations: Reservation[];
}

const WEEKDAYS = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday",
];
const DaysColumn = ({ reservations, index, hour }: DaysColumnProps) => {
  const [isPopoverVisible, setPopoverVisible] = useState(false);
  const getReservationDuration = (reservation: Reservation) => {
    const time =
      new Date(reservation.endTime).getTime() -
      new Date(reservation.startTime).getTime();
    return time / 3600000;
  };

  const getReservationHeight = (reservation: Reservation) => {
    return getReservationDuration(reservation) * 100;
  };

  const getTopOffset = (reservation: Reservation) => {
    const reservationStartTime = utcToZonedTime(reservation.startTime, "CET");
    const reservationMinutes = reservationStartTime.getMinutes();
    console.log((reservationMinutes / 60) * 100);
    return (reservationMinutes / 60) * 100;
  };

  const filteredReservations = reservations.filter((reservation) => {
    const reservationDate = utcToZonedTime(reservation.startTime, "CET");
    return (
      format(reservationDate, "EEEE") === WEEKDAYS[index] &&
      reservationDate.getHours() === hour
    );
  });

  return (
    <div className="border">
      {filteredReservations.map((reservation, index) => (
        <div key={`reservation-${index}`} className="relative h-full">
          <div
            onMouseEnter={() => setPopoverVisible(true)}
            onMouseLeave={() => setPopoverVisible(false)}
            className="absolute left-0 top-0 right-0 bottom-0 bg-red-400 text-white z-2 opacity-90 rounded"
            style={{
              height: `calc(${getReservationHeight(reservation)}% + ${
                (getReservationDuration(reservation) - 1) * 2
              }px)`,
              top: `calc(${getTopOffset(reservation)}%)`,
            }}
          ></div>
          <Popover isVisible={isPopoverVisible} reservation={reservation} />
        </div>
      ))}
    </div>
  );
};

export default DaysColumn;
