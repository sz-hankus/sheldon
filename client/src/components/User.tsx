import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { getUser } from "../api/usersApiClient.ts";
import { User } from "../models/User.ts";
import useAuth from "../hooks/useAuth.ts";

const User = () => {
  const [user, setUser] = useState<User | null>(null);
  const { id } = useParams();

  const { auth } = useAuth();

  useEffect(() => {
    const fetchUser = async () => {
      return await getUser(Number(id) ?? "", auth?.jwtAccessToken ?? "");
    };
    if (!isNaN(Number(id || ""))) fetchUser().then((user) => setUser(user));
  }, [id, auth?.jwtAccessToken]);

  return (
    // TODO: Zrobić tu coś porządniejszego
    <>
      <section className="flex flex-col">
        <h1 className="text-xl font-bold">Użytkownik</h1>
        <span>Email: {user?.email}</span>
        <span>Rola: {user?.role}</span>
        <span>Utworzony: {user?.creationTime}</span>
      </section>
      <Link to="/">Strona główna</Link>
    </>
  );
};

export default User;
