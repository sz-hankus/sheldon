import { useEffect, useState } from "react";
import { Station, getUrls } from "../../models/Station";
import { useParams } from "react-router-dom";
import useAuth from "../../hooks/useAuth";
import { getStation } from "../../api/stationsApiClient";
import ImageSlides from "../ImageSlides";

const Station = () => {
  const [station, setStation] = useState<Station | null>(null);

  const { id } = useParams();
  const { auth } = useAuth();

  useEffect(() => {
    const fetchStation = async () => {
      return await getStation(Number(id) ?? "", auth?.jwtAccessToken ?? "");
    };
    if (!isNaN(Number(id || "")))
      fetchStation().then((station) => setStation(station));
  }, [id, auth?.jwtAccessToken]);

  return (
    <section className="flex flex-col flex-grow px-6 py-3 bg-gray-50">
      <div className="mx-auto border bg-white max-w-xl rounded-xl overflow-hidden">
        <ImageSlides
          urls={getUrls(station?.image_refs || null)}
        />
        <div className="flex flex-col flex-grow justify-between p-5">
          <a href="#">
            <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">
              {station?.name}
            </h5>
          </a>
          <p className="mb-3 font-normal text-gray-700 ">
            {station?.description}
          </p>
        </div>
      </div>
    </section>
  );
};

export default Station;
