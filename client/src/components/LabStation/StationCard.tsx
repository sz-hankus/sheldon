import { Link } from "react-router-dom";
import { Station, getUrls } from "../../models/Station";
import ImageSlides from "../ImageSlides";

export interface StationCardProps {
  station: Station;
}

const StationCard = ({ station }: StationCardProps) => {
  return (
    <div className="flex flex-col min-w-96 max-w-sm bg-white border border-gray-200 rounded-lg shadow">
      <ImageSlides urls={getUrls(station.image_refs)}/>
      <div className="flex flex-col flex-grow justify-between p-5">
        <a href="#">
          <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">
            {station.name}
          </h5>
        </a>
        <p className="mb-3 font-normal text-gray-700 ">{station.description}</p>
        <div className="flex text-center space-x-4">
          <button className="px-3 py-2 w-full font-medium text-white text-sm bg-blue-600 rounded-md hover:bg-blue-700">
            Zarezerwuj
          </button>
          <Link to={`/lab-stations/${station.id}`} className="w-full">
            <button className="px-3 py-2 w-full font-medium text-sm rounded-md border border-gray-300 hover:bg-gray-100">
              Szczegóły
            </button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default StationCard;
