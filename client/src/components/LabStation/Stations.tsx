import { useEffect, useState } from "react";
import useAuth from "../../hooks/useAuth";
import { getStations } from "../../api/stationsApiClient";
import { Station } from "../../models/Station";
import StationCard from "./StationCard";

const Stations = () => {
  const [stations, setStations] = useState<Station[]>([]);

  const { auth } = useAuth();

  useEffect(() => {
    const fetchStations = async () => {
      return await getStations(auth?.jwtAccessToken ?? "");
    };

    fetchStations().then((stations) => setStations(stations));
  }, [auth?.jwtAccessToken]);

  return (
    <section className="flex flex-col flex-grow px-6 py-3 bg-gray-50">
      <div className="grid grid-cols-1 min-[872px]:grid-cols-2 xl:grid-cols-3 gap-x-14 gap-y-6 mx-auto mt-3 min-[1752px]:grid-cols-4">
        {stations?.map((station: Station) => (
          <StationCard key={station.id} station={station} />
        ))}
      </div>
    </section>
  );
};

export default Stations;
