import useAuth from "../hooks/useAuth.ts";
import { Navigate, Outlet } from "react-router-dom";
import { UserRoles } from "../models/UserRoles.tsx";

interface RequireAuthProps {
  allowedRoles: UserRoles[];
}

const RequireAuth = ({ allowedRoles }: RequireAuthProps) => {
  const { getRole, isTokenExpired } = useAuth();
  const isRoleAllowed = allowedRoles.includes(getRole());

  return isRoleAllowed && !isTokenExpired() ? (
    <Outlet />
  ) : (
    <Navigate to="/users/login" replace />
  );
};

export default RequireAuth;
