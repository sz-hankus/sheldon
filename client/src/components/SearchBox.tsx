import SearchIcon from "../icons/SearchIcon"

interface SearchBoxProps {
    onChange: (searchValue: string) => void
}

const SearchBox = ({ onChange }: SearchBoxProps) => {
  return (
    <div className="flex flex-row divide-x divide-gray-400 space-x-3 bg-slate p-2 mt-2 border-2 border-gray-300 rounded-2xl">
      <SearchIcon strokeWidth={1.5} className="ml-1 w-6 h-6 text-gray-500"/>
      <div className="" />
      <input onChange={(event) => onChange(event.target.value)} className="grow bg-transparent outline-none border-none text-slate-700"></input>
    </div>
  )
}

export default SearchBox