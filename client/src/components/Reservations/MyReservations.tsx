import { useEffect, useState } from "react";
import { getReservations } from "../../api/reservationApiClient.ts";
import useAuth from "../../hooks/useAuth.ts";
import { Reservation } from "../../models/Reservation.ts";
import { format } from "date-fns";
import { utcToZonedTime } from "date-fns-tz";
import { getStation } from "../../api/stationsApiClient.ts";

const MyReservations = () => {
  const [reservations, setReservations] = useState<Reservation[]>([]);
  const [stationNames, setStationNames] = useState<{ [key: number]: string }>(
    {},
  );

  const { auth, getId } = useAuth();

  useEffect(() => {
    const fetchReservations = async () => {
      const allReservations = await getReservations(auth?.jwtAccessToken ?? "");
      return allReservations.filter(
        (reservation: Reservation) => reservation.reserverId === getId(),
      );
    };

    fetchReservations().then((reservations) => setReservations(reservations));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [auth?.jwtAccessToken]);

  useEffect(() => {
    const fetchStationNames = async () => {
      const names: { [key: number]: string } = {};
      for (const reservation of reservations) {
        if (reservation.labStationId !== null) {
          const station = await getStation(
            Number(reservation.labStationId),
            auth?.jwtAccessToken ?? "",
          );
          names[reservation.labStationId] = station?.name;
        }
      }
      return names;
    };

    fetchStationNames().then((names) => setStationNames(names));
  }, [reservations, auth?.jwtAccessToken]);

  return (
    <section className="flex items-center justify-center flex-grow px-4 sm:px-0 w-full">
      <div className="flex flex-col w-full max-w-md bg-white border border-gray-200 rounded-lg shadow">
        <div className="flex flex-col flex-grow justify-between p-5">
          {reservations.length !== 0 ? (
            <>
              <a href="#">
                <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">
                  Moje rezerwacje
                </h5>
              </a>
              <ul className="max-w-md divide-y divide-gray-200">
                {reservations?.map((reservation: Reservation) => (
                  <li className="py-3 sm:py-4">
                    <div className="flex items-center space-x-5">
                      <div className="flex-1 min-w-0">
                        <p className="text-md font-semibold text-gray-800">
                          {reservation.labStationId !== null
                            ? stationNames[reservation.labStationId]
                            : ""}
                        </p>
                        <p className="text-sm text-gray-500">
                          {format(
                            utcToZonedTime(reservation.startTime, "CET"),
                            "dd-MM HH:mm",
                          )}{" "}
                          -{" "}
                          {format(
                            utcToZonedTime(reservation.endTime, "CET"),
                            "HH:mm",
                          )}
                        </p>
                      </div>
                      <div className="inline-flex items-center">
                        <button className="px-3 py-2 w-full font-medium text-sm rounded-md border text-white bg-blue-600 hover:bg-blue-800">
                          Szczegóły
                        </button>
                      </div>
                    </div>
                  </li>
                ))}
              </ul>
            </>
          ) : (
            <a href="#">
              <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">
                Brak rezerwacji
              </h5>
            </a>
          )}
        </div>
      </div>
    </section>
  );
};

export default MyReservations;
