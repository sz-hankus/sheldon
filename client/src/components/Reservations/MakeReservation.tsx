import React, { useEffect, useState } from "react";
import { ReservationForm } from "../../models/Reservation.ts";
import { createReservation } from "../../api/reservationApiClient.ts";
import useAuth from "../../hooks/useAuth.ts";
import { getStations } from "../../api/stationsApiClient.ts";
import { Station } from "../../models/Station.ts";
import WeekCalendar from "../Calendar/WeekCalendar.tsx";

const MakeReservation = () => {
  const [reservation, setReservation] = useState<ReservationForm>({
    labStationId: 0,
    startTime: "",
    endTime: "",
    status: "",
  });
  const [stations, setStations] = useState<Station[]>([]);
  const [startTimeError, setStartTimeError] = useState<boolean>(false);
  const [endTimeError, setEndTimeError] = useState<boolean>(false);

  const { auth } = useAuth();

  useEffect(() => {
    const fetchStations = async () => {
      const fetchedStations = await getStations(auth?.jwtAccessToken ?? "");
      if (fetchedStations.length > 0) {
        setReservation((prevReservation) => ({
          ...prevReservation,
          labStationId: fetchedStations[0].id,
        }));
      }
      return fetchedStations;
    };

    fetchStations().then((stations) => setStations(stations));
  }, [auth?.jwtAccessToken]);

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    setStartTimeError(false);
    setEndTimeError(false);

    const now = new Date();
    const startTime = new Date(reservation.startTime);
    const endTime = new Date(reservation.endTime);

    if (startTime < now) {
      setStartTimeError(true);
      return;
    }

    if (endTime < startTime) {
      setEndTimeError(true);
      return;
    }

    const formattedStartTime = startTime.toISOString();
    const formattedEndTime = endTime.toISOString();

    const updatedReservation: ReservationForm = {
      ...reservation,
      startTime: formattedStartTime,
      endTime: formattedEndTime,
      labStationId: parseInt(reservation.labStationId.toString()),
    };

    try {
      await createReservation(auth?.jwtAccessToken ?? "", updatedReservation);
      //temporary ofc, just for testings
      window.location.reload();
    } catch (err) {
      console.log(err);
    }
  };

  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>,
  ) => {
    const { name, value } = e.target;
    console.log(name, value);
    setReservation((prevReservation) => ({
      ...prevReservation,
      [name]: value,
    }));
  };

  const handleSelfReservationCheckbox = (
    e: React.ChangeEvent<HTMLInputElement>,
  ) => {
    setReservation((prevReservation) => ({
      ...prevReservation,
      status: e.target.checked ? "pending_stage2" : "",
    }));
  };

  return (
    <section className="flex py-6 md:py-0 md:items-center justify-center flex-grow px-4 sm:px-0">
      <div className="flex flex-col md:flex-row bg-gray-50 w-full sm:max-w-3xl rounded-lg shadow-lg max-h-[520px]">
        <div className="p-8 space-y-6 w-11/12">
          <h1 className="text-2xl font-bold">Nowa rezerwacja</h1>
          <form onSubmit={handleSubmit} className="space-y-4">
            <div>
              <label htmlFor="lab-station" className="block mb-2 font-medium">
                Stanowisko laboratoryjne
              </label>
              <select
                id="lab-station"
                value={reservation.labStationId}
                name="labStationId"
                onChange={handleChange}
                required
                className="bg-white border-2 border-gray-300 rounded-lg block p-2.5 w-full"
              >
                {stations?.map((station: Station) => (
                  <option key={station.id} value={station.id}>
                    {station.name}
                  </option>
                ))}
              </select>
            </div>
            <div>
              <label htmlFor="start-time" className="block mb-2 font-medium">
                Czas rozpoczęcia
              </label>
              <input
                type="datetime-local"
                id="start-time"
                name="startTime"
                value={reservation.startTime}
                onChange={handleChange}
                required
                className="bg-white border-2 border-gray-300 rounded-lg block p-2.5 w-full"
                step="900"
              />
            </div>
            {startTimeError && (
              <div className="p-4 mb-4 text-sm text-red-800 rounded-lg bg-red-50">
                Rezerwacja nie może zaczynać się w przeszłości
              </div>
            )}
            <div>
              <label htmlFor="end-time" className="block mb-2 font-medium">
                Czas zakończenia
              </label>
              <input
                type="datetime-local"
                id="end-time"
                name="endTime"
                value={reservation.endTime}
                onChange={handleChange}
                required
                className="bg-white border-2 border-gray-300 rounded-lg block p-2.5 w-full"
                step="900"
              />
              {endTimeError && (
                <div className="p-4 mt-4 text-sm text-red-800 rounded-lg bg-red-50">
                  Rezerwacja nie może kończyć się przed rozpoczęciem
                </div>
              )}
            </div>
            <div className="flex items-center ps-2">
              <input
                type="checkbox"
                id="self-reservation"
                name="status"
                onChange={handleSelfReservationCheckbox}
                className="w-4 h-4 accent-blue-700"
              />
              <label
                htmlFor="self-reservation"
                className="w-full py-0.5 ms-3 text-sm text-gray-900"
              >
                Chcę samemu obsłużyć rezerwację
              </label>
            </div>
            <button className="w-full text-white bg-blue-700 hover:bg-blue-900 font-medium rounded-lg text-center py-2.5">
              Zarezerwuj
            </button>
          </form>
        </div>
        <WeekCalendar stationFilter={reservation.labStationId} />
      </div>
    </section>
  );
};

export default MakeReservation;
