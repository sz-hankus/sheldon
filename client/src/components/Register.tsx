import React, { useMemo, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { UserForm } from "../models/User.ts";
import { registerUser } from "../api/usersApiClient.ts";

const Register = () => {
  const [credentials, setCredentials] = useState<UserForm>({
    email: "",
    password: "",
    role: "reserver",
  });
  const [profilePicture, setProfilePicture] = useState<File>();
  const [registrationError, setRegistrationError] = useState<boolean>(false);
  const pictureUrl = useMemo(() => {
    if (profilePicture !== undefined)
      return URL.createObjectURL(profilePicture);
    return "";
  }, [profilePicture]);

  const navigate = useNavigate();

  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    const { name, value } = e.target;
    setCredentials((prevCredentials) => ({
      ...prevCredentials,
      [name]: value,
    }));
  };

  const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const selectedFile = e.target.files?.item(0);
    selectedFile !== null
      ? setProfilePicture(selectedFile)
      : setProfilePicture(undefined);
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    try {
      await registerUser(credentials, profilePicture);
      setCredentials({ email: "", password: "", role: "reserver" });
      setRegistrationError(false);
      navigate("/");
    } catch {
      setRegistrationError(true);
    }
  };

  return (
    <section className="flex items-center justify-center flex-grow px-4 sm:px-0">
      <div className="bg-gray-50 w-full sm:max-w-md rounded-lg shadow-lg">
        <div className="p-8 space-y-6">
          <h1 className="text-2xl font-bold">Utwórz konto</h1>
          <form
            onSubmit={handleSubmit}
            className="space-y-4"
            data-testid="submit-form"
          >
            <div>
              <label
                htmlFor="registration-email"
                className="block mb-2 font-medium"
              >
                Email
              </label>
              <input
                type="email"
                id="registration-email"
                data-testid="registration-email"
                name="email"
                value={credentials.email}
                onChange={handleChange}
                placeholder="example@mail.com"
                required
                className="border-2 border-gray-300 rounded-lg block p-2.5 w-full"
              />
            </div>
            <div>
              <label
                htmlFor="registration-password"
                className="block mb-2 font-medium"
              >
                Hasło
              </label>
              <input
                type="password"
                id="registration-password"
                data-testid="registration-password"
                name="password"
                placeholder="********"
                value={credentials.password}
                onChange={handleChange}
                minLength={8}
                required
                className="border-2 border-gray-300 rounded-lg block p-2.5 w-full"
              />
            </div>
            <div>
              <label htmlFor="role" className="block mb-2 font-medium">
                Rola
              </label>
              <select
                id="role"
                data-testid="role"
                value={credentials.role}
                name="role"
                onChange={handleChange}
                required
                className="bg-white border-2 border-gray-300 rounded-lg block p-2.5 w-full"
              >
                <option value="reserver">Rezerwujący</option>
                <option value="lab_master">Technik</option>
                <option value="admin">Admin</option>
              </select>
            </div>
            <div>
              <label className="block mb-2 font-medium">
                Zdjęcie profilowe
              </label>
              <div className="flex flex-row items-center border-gray-300 border-2 rounded-lg bg-white p-2.5 w-full">
                <input
                  data-testid="profile-picture"
                  type="file"
                  accept="image/png, image/jpeg"
                  onChange={handleFileChange}
                  multiple={false}
                  className=""
                />
                <div className="aspect-square max-h-16 flex align-center">
                  <img
                    src={pictureUrl}
                    className={`${
                      pictureUrl
                        ? "object-cover w-16 rounded-full border-2 border-gray-600"
                        : ""
                    }`}
                  />
                </div>
              </div>
            </div>
            <button className="w-full text-white bg-blue-700 hover:bg-blue-900 font-medium rounded-lg text-center py-2.5">
              Zarejestruj się
            </button>
            {registrationError && (
              <div className="p-4 mb-4 text-sm text-red-800 rounded-lg bg-red-50">
                Błąd! Spróbuj ponownie
              </div>
            )}
            <p className="text-sm pt-2">
              Masz już konto?
              <Link
                to="/users/login"
                className="ml-2 text-sm text-blue-700 hover:text-blue-900 font-medium"
              >
                Zaloguj się
              </Link>
            </p>
          </form>
        </div>
      </div>
    </section>
  );
};

export default Register;
