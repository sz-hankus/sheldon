import { RenderResult, fireEvent, render } from "@testing-library/react";
import Register from "../components/Register";
import { MemoryRouter } from "react-router-dom";

describe("Register", () => {
  let utils: RenderResult;

  beforeEach(() => {
    utils = render(
      <MemoryRouter>
        <Register />
      </MemoryRouter>
    );
  });

  it("renders properly", () => {
    const { getByText } = utils;
    expect(getByText("Utwórz konto")).toBeInTheDocument();
  });

  it("email input field accepts input", () => {
    const { getByTestId } = utils;

    const emailInput = getByTestId("registration-email");
    fireEvent.change(emailInput, { target: { value: "example@email.com" } });
    expect(emailInput).toHaveValue("example@email.com");
  });

  it("password input field accepts input", () => {
    const { getByTestId } = utils;

    const passwordInput = getByTestId("registration-password");
    fireEvent.change(passwordInput, { target: { value: "password" } });
    expect(passwordInput).toHaveValue("password");
  });

  it("email field is required", () => {
    const { getByTestId } = utils;

    const emailInput = getByTestId("registration-email");
    fireEvent.change(emailInput, { target: { value: "" } });
    expect(emailInput).toBeInvalid();
  });

  it("password field is required", () => {
    const { getByTestId } = utils;

    const passwordInput = getByTestId("registration-password");
    fireEvent.change(passwordInput, { target: { value: "" } });
    expect(passwordInput).toBeInvalid();
  });

  it("email field accepts valid email", () => {
    const { getByTestId } = utils;

    const emailInput = getByTestId("registration-email");
    fireEvent.change(emailInput, { target: { value: "email" } });
    expect(emailInput).toBeInvalid();

    fireEvent.change(emailInput, { target: { value: "email@" } });
    expect(emailInput).toBeInvalid();

    fireEvent.change(emailInput, { target: { value: "email@mail.com" } });
    expect(emailInput).toBeValid();
  });

  it("selecting role changes option", () => {
    const { getByTestId } = utils;

    const roleSelect = getByTestId("role");
    fireEvent.change(roleSelect, { target: { value: "admin" } });
    expect(roleSelect).toHaveValue("admin");
  });
});
