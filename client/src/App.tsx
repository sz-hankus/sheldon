import "./App.css";
import Register from "./components/Register.tsx";
import Login from "./components/Login.tsx";
import { Route, Routes } from "react-router-dom";
import RequireAuth from "./components/RequireAuth.tsx";
import Index from "./components/Index.tsx";
import User from "./components/User.tsx";
import { UserRoles } from "./models/UserRoles.ts";
import Navbar from "./components/Navbar/Navbar.tsx";
import Users from "./components/Users.tsx";
import Stations from "./components/LabStation/Stations.tsx";
import Station from "./components/LabStation/Station.tsx";
import MakeReservation from "./components/Reservations/MakeReservation.tsx";

function App() {
  return (
    <>
      <Navbar />
      <Routes>
        <Route path="/users/register" element={<Register />} />
        <Route path="/users/login" element={<Login />} />

        <Route
          element={
            <RequireAuth
              allowedRoles={[
                UserRoles.Reserver,
                UserRoles.LabMaster,
                UserRoles.Admin,
                UserRoles.SuperAdmin,
              ]}
            />
          }
        >
          <Route path="/" element={<Index />} />
          <Route path="/users/:id" element={<User />} />
          <Route path="/lab-stations/:id" element={<Station />} />
          <Route path="/lab-stations" element={<Stations />} />
          <Route path="/reservations" element={<MakeReservation />} />
        </Route>

        <Route
          element={
            <RequireAuth
              allowedRoles={[UserRoles.Admin, UserRoles.SuperAdmin]}
            />
          }
        >
          <Route path="/users" element={<Users />} />
        </Route>
      </Routes>
    </>
  );
}

export default App;
