export type Station = {
  id: number;
  name: string;
  description: string;
  image_refs: string[] | null; // TODO: spojrzeć jeszcze na to po stronie backendu
};

export const getUrls = (image_refs: string[] | null) => {
  return image_refs ? image_refs.map(uuid => `${window.location.origin}/static/${uuid}`) : []
}
