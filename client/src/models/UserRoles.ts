export enum UserRoles {
  Reserver = "reserver",
  LabMaster = "lab_master",
  Admin = "admin",
  SuperAdmin = "super_admin",
}
