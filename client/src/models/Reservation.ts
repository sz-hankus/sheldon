export type Reservation = {
  id: number;
  reserverId: number | null;
  labMasterId: number | null;
  labStationId: number | null;
  status: string;
  creationTime: string;
  startTime: string;
  endTime: string;
  chatLog: string;
};

export type ReservationForm = {
  labStationId: number;
  startTime: string;
  endTime: string;
  status: string;
};
