export type User = {
  id: number;
  email: string;
  role: string;
  creationTime: string;
  imageRef: string;
};

export type UserForm = {
  email: string;
  password: string;
  role?: string;
};
