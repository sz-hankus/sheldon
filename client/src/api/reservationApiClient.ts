import axios from "../api/apiConfig.ts";
import { ReservationForm } from "../models/Reservation.ts";

export const getReservations = async (
  jwtAccessToken: string,
  stationFilter?: number,
) => {
  try {
    const response = await axios.get(`/reservations`, {
      headers: { Authorization: `Bearer ${jwtAccessToken}` },
      params: { labStation: stationFilter ?? "" },
    });
    return response.data;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

export const createReservation = async (
  jwtAccessToken: string,
  reservationData: ReservationForm,
) => {
  console.log(JSON.stringify(reservationData));
  try {
    const response = await axios.post(
      `/reservations`,
      JSON.stringify(reservationData),
      {
        headers: { Authorization: `Bearer ${jwtAccessToken}` },
      },
    );
    return response.data;
  } catch (err) {
    console.log(err);
    throw err;
  }
};
