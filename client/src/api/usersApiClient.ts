import axios from "./apiConfig.ts";
import { User, UserForm } from "../models/User.ts";

export const getUser = async (id: number, jwtAccessToken: string) => {
  try {
    const response = await axios.get(`/users/${id}`, {
      headers: { Authorization: `Bearer ${jwtAccessToken}` },
    });
    return response.data;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

export const getUsers = async (jwtAccessToken: string, emailFilter?: string) => {
  try {
    const response = await axios.get(`/users`, {
      headers: { Authorization: `Bearer ${jwtAccessToken}` },
      params: { "email": emailFilter ?? "" }
    });
    return response.data;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

export const loginUser = async (credentials: UserForm) => {
  try {
    const response = await axios.post(
      "/users/login",
      JSON.stringify(credentials),
      { headers: { "Content-Type": "application/json" } }
    );
    return response.data;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

export const registerUser = async (credentials: UserForm, profilePicture?: File) => {
  try {
    const formData = new FormData();
    formData.append("credentials", JSON.stringify(credentials));
    if (profilePicture) {
      formData.append("profile-picture", profilePicture);
    }
    await axios.post("/users", formData, {
      headers: { "Content-Type": "multipart/form-data" },
    });
  } catch (err) {
    console.log(err);
    throw err;
  }
};

export const deleteUser = async (id: number, jwtAccessToken: string) => {
  try {
    await axios.delete(`/users/${id}`, {
      headers: { Authorization: `Bearer ${jwtAccessToken}` },
    });
  } catch (err) {
    console.log(err);
    throw err;
  }
};

export const updateUser = async (
  id: number,
  jwtAccessToken: string,
  userDetails: User
) => {
  try {
    await axios.put(`/users/${id}`, JSON.stringify(userDetails), {
      headers: { Authorization: `Bearer ${jwtAccessToken}` },
    });
  } catch (err) {
    console.log(err);
    throw err;
  }
};
