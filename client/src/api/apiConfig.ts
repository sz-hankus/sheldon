import axios from "axios";

export default axios.create({
  // Environment variable used in the Dockerfile
  baseURL: `${import.meta.env.VITE_ENV == "docker" ? window.location.origin + "/api" : "http://localhost:8080" }`,
});
