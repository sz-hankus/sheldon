import axios from "./apiConfig.ts";

export const getStations = async (jwtAccessToken: string) => {
  try {
    const response = await axios.get(`/lab-stations`, {
      headers: { Authorization: `Bearer ${jwtAccessToken}` },
    });
    return response.data;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

export const getStation = async (id: number, jwtAccessToken: string) => {
  try {
    const response = await axios.get(`/lab-stations/${id}`, {
      headers: { Authorization: `Bearer ${jwtAccessToken}` },
    });
    return response.data;
  } catch (err) {
    console.log(err);
    throw err;
  }
};
