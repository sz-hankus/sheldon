package config

import (
	"flag"
	"fmt"
	"os"
	"sheldonAPI/internal/log"

	"gopkg.in/yaml.v3"
)

type SheldonConfig struct {
	Server struct {
		Port int `yaml:"port"`
	}

	Database struct {
		Driver   string `yaml:"driver"`
		User     string `yaml:"user"`
		Password string `yaml:"password"`
		Host     string `yaml:"host"`
		Port     int    `yaml:"port"`
		DbName   string `yaml:"dbName"`
		SslMode  bool   `yaml:"sslMode"`
	}

	Static struct {
		BasePath string `yaml:"basePath"`
	}
}

var (
	config SheldonConfig
)

func init() {
	confPath := flag.String("config", "", "path to the config file")
	flag.Parse()
	log.Logger.Debug(fmt.Sprintf("Reading config at %q", *confPath))

	file, err := os.Open(*confPath)
	if err != nil {
		panic(fmt.Errorf("fatal error reading config file at %q : %w", *confPath, err))
	}

	err = yaml.NewDecoder(file).Decode(&config)
	if err != nil {
		panic(fmt.Errorf("fatal error decoding config file at %q : %w", *confPath, err))
	}
}

func GetConfig() SheldonConfig {
	return config
}
