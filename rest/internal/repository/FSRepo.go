package repository

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sheldonAPI/internal/log"

	"github.com/google/uuid"
)

// A repository for saving files on the filesystem

type FSRepo struct {
	BasePath string
}

func (repo *FSRepo) Save(reader io.Reader) (uuid.UUID, error) {
	id, err := uuid.NewUUID()
	if err != nil {
		log.Logger.Error("Could not create a uuid", "cause", err)
		return uuid.UUID{}, err
	}
	path := filepath.Join(repo.BasePath, id.String())
	newFile, err := os.Create(path)
	if err != nil {
		log.Logger.Error("Could not create a new file", "cause", err)
		return uuid.UUID{}, err
	}

	written, err := io.Copy(newFile, reader)
	if err != nil {
		log.Logger.Error("Could not create a new file", "cause", err)
		return uuid.UUID{}, err
	}
	log.Logger.Info(fmt.Sprintf("Wrote %d bytes to %s", written, path))
	return id, nil
}
