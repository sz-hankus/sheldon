package repository

import (
	"database/sql"
	"fmt"
	"os"
	"sheldonAPI/internal/config"
	"sheldonAPI/internal/log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var (
	db *sqlx.DB
)

func GetDbUrl(conf config.SheldonConfig) string {
	var ssl string
	if conf.Database.SslMode {
		ssl = "enable"
	} else {
		ssl = "disable"
	}

	return fmt.Sprintf(
		"%s://%s:%s@%s:%d/%s?sslmode=%s",
		conf.Database.Driver,
		conf.Database.User,
		conf.Database.Password,
		conf.Database.Host,
		conf.Database.Port,
		conf.Database.DbName,
		ssl,
	)
}

func init() {
	conf := config.GetConfig()
	connUrl := GetDbUrl(conf)
	log.Logger.Info(fmt.Sprintf("Database init started. Using url: %s", connUrl))
	base, err := sql.Open("postgres", connUrl)
	if err != nil {
		log.Logger.Error("Failed to access database", "cause", err)
		os.Exit(2) // TODO: maybe define different exit codes for different reasons
	}

	db = sqlx.NewDb(base, "postgres")

	err = db.Ping()
	if err != nil {
		log.Logger.Error("Failed to ping database", "cause", err)
		os.Exit(2)
	}
}

func GetDB() *sqlx.DB {
	return db
}

func CloseDB() {
	log.Logger.Info("Closing db...")
	err := db.Close()
	if err != nil {
		log.Logger.Error("Closing db failed. Oh hell nah.")
		return
	}
	log.Logger.Info("Db closed successfully. Yay.")
}
