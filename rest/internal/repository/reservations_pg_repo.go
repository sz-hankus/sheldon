package repository

import (
	"sheldonAPI/internal/domain/model"
	"sheldonAPI/internal/log"

	"github.com/jmoiron/sqlx"
)

type ReservationRepo struct {
	DB *sqlx.DB
}

func (r *ReservationRepo) FindAll() ([]model.Reservation, error) {
	s := `
	SELECT * FROM reservations;
	`
	reservations := []model.Reservation{}
	err := r.DB.Select(&reservations, s)
	if err != nil {
		log.Logger.Error("Error when fetching from db", "error", err)
		return []model.Reservation{}, err
	}
	return reservations, nil
}

func (r *ReservationRepo) FindByLabStationId(id uint) ([]model.Reservation, error) {
	s := `
	SELECT * FROM reservations
	WHERE lab_station_id=$1;
	`
	var reservations []model.Reservation
	err := r.DB.Select(&reservations, s, id)
	if err != nil {
		log.Logger.Error("Error when fetching from db", "error", err)
		return []model.Reservation{}, err
	}
	return reservations, nil
}

func (r *ReservationRepo) Find(id uint) (model.Reservation, error) {
	s := `
	SELECT * FROM reservations
	WHERE reservation_id=$1;
	`
	var rsrv model.Reservation
	err := r.DB.QueryRowx(s, id).StructScan(&rsrv)
	if err != nil {
		log.Logger.Error("Error when fetching from db", "error", err)
		return model.Reservation{}, err
	}
	return rsrv, nil
}

func (r *ReservationRepo) Save(rsrv model.Reservation) (model.Reservation, error) {
	s := `
	INSERT INTO reservations
	(reserver_id, lab_master_id, lab_station_id, status, created_on, start_time, end_time, chat_log)
	VALUES (:reserver_id, :lab_master_id, :lab_station_id, :status, :created_on, :start_time, :end_time, :chat_log)
	RETURNING *;
	`
	var created model.Reservation
	stmt, err := r.DB.PrepareNamed(s)
	if err != nil {
		log.Logger.Error("Error when preparing a named statement", "error", err)
		return model.Reservation{}, err
	}

	err = stmt.Get(&created, rsrv)
	if err != nil {
		log.Logger.Error("Error when executing db statment", "error", err)
		return model.Reservation{}, err
	}
	return created, nil
}

func (r *ReservationRepo) Update(rsrv model.Reservation) (model.Reservation, error) {
	s := `
	UPDATE reservations SET
	reserver_id=:reserver_id, 
	lab_master_id=:lab_master_id, 
	lab_station_id=:lab_station_id, 
	status=:status,
	created_on=:created_on,
	start_time=:start_time,
	end_time=:end_time,
	chat_log=:chat_log
	WHERE reservation_id=:reservation_id
	RETURNING *;
	`
	var updated model.Reservation
	stmt, err := r.DB.PrepareNamed(s)
	if err != nil {
		log.Logger.Error("Error when preparing a named statement", "error", err)
		return model.Reservation{}, err
	}

	err = stmt.Get(&updated, rsrv)
	if err != nil {
		log.Logger.Error("Error when executing db statment", "error", err)
		return model.Reservation{}, err
	}
	return updated, nil
}
