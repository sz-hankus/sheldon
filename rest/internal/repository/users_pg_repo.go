package repository

import (
	"sheldonAPI/internal/domain/model"
	"sheldonAPI/internal/domain/usecase"
	"sheldonAPI/internal/log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type UserRepo struct {
	DB *sqlx.DB
}

func (r *UserRepo) Save(u model.User) (model.User, error) {
	s := `
	INSERT INTO users
	(email, password, role, created_on, image_ref) VALUES 
	(:email, :password, :role, :created_on, :image_ref)
	RETURNING user_id;`
	stmt, err := r.DB.PrepareNamed(s)
	if err != nil {
		return model.User{}, err
	}
	var id uint
	err = stmt.Get(&id, u)
	if err != nil {
		return model.User{}, err
	}
	u.Id = uint64(id)
	log.Logger.Info("Saved user to database", "user", u)
	return u, nil
}

func (r *UserRepo) FindAll() ([]model.User, error) {
	s := `
	SELECT * FROM users;
	`
	users := []model.User{}
	err := r.DB.Select(&users, s)
	if err != nil {
		return []model.User{}, err
	}
	return users, nil
}

func (r *UserRepo) Find(id uint) (model.User, error) {
	s := `
	SELECT * FROM users
	WHERE user_id=$1;
	`
	var u model.User
	err := r.DB.QueryRowx(s, id).StructScan(&u)
	if err != nil {
		return model.User{}, err
	}
	return u, nil
}

func (r *UserRepo) FuzzyFind(query string) ([]model.User, error) {
	s := `
	SELECT * FROM users
	WHERE similarity(email, $1) > 0.1
	ORDER BY similarity(email, $1) DESC;
	`
	users := []model.User{}
	err := r.DB.Select(&users, s, query)
	if err != nil {
		return []model.User{}, err
	}
	return users, nil
}

func (r *UserRepo) Search(email string) ([]model.User, error) {
	s := `
	SELECT * FROM users
	WHERE email=$1;
	`
	users := []model.User{}
	err := r.DB.Select(&users, s, email)
	if err != nil {
		return []model.User{}, err
	}
	return users, nil
}

func (r *UserRepo) Update(id uint, u model.User) (model.User, error) {
	s := `
	UPDATE users
	SET email=:email, role=:role
	WHERE user_id=:id;
	`
	stmt, err := r.DB.PrepareNamed(s)
	if err != nil {
		return model.User{}, err
	}

	params := map[string]interface{}{
		"email": u.Email,
		"role":  u.Role,
		"id":    id,
	}

	res, err := stmt.Exec(params)
	if err != nil {
		return model.User{}, err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil || rowsAffected == 0 {
		return model.User{}, usecase.ErrUserNotFound
	}

	return u, nil
}

func (r *UserRepo) Delete(id uint) (model.User, error) {
	s := `
	DELETE FROM users
	WHERE user_id=$1
	RETURNING *;
	`
	var u model.User
	err := r.DB.QueryRowx(s, id).StructScan(&u)
	if err != nil {
		return model.User{}, err
	}
	return u, nil
}
