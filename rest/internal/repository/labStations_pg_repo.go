package repository

import (
	"sheldonAPI/internal/domain/model"
	"sheldonAPI/internal/log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type LabStationRepo struct {
	DB *sqlx.DB
}

func (r *LabStationRepo) FindAll() ([]model.LabStation, error) {
	s := `
	SELECT * FROM lab_stations;
	`
	stations := []model.LabStation{}
	err := r.DB.Select(&stations, s)
	if err != nil {
		log.Logger.Error("Error when fetching from db", "error", err)
		return []model.LabStation{}, err
	}
	return stations, nil
}

func (r *LabStationRepo) Find(id uint) (model.LabStation, error) {
	s := `
	SELECT * FROM lab_stations
	WHERE labstation_id=$1;
	`
	var ls model.LabStation
	err := r.DB.QueryRowx(s, id).StructScan(&ls)
	if err != nil {
		log.Logger.Error("Error when fetching from db", "error", err)
		return model.LabStation{}, err
	}
	return ls, nil
}

func (r *LabStationRepo) Save(ls model.LabStation) (model.LabStation, error) {
	s := `
	INSERT INTO lab_stations
	(name, description, image_refs) VALUES
	(:name, :description, :image_refs)
	RETURNING labstation_id;`
	stmt, err := db.PrepareNamed(s)
	if err != nil {
		log.Logger.Error("Error when preparing a named stament for db", "error", err)
		return model.LabStation{}, err
	}
	var id uint
	err = stmt.Get(&id, ls)
	if err != nil {
		log.Logger.Error("Error when inserting into db", "error", err)
		return model.LabStation{}, err
	}
	ls.Id = uint64(id)
	log.Logger.Info("Saved lab station to database", "labStaion", ls)
	return ls, nil
}
