package router

import (
	"github.com/gorilla/mux"
)

type Registerer interface {
	RegisterEndpoints(r *mux.Router)
}

func NewRouter(controllers ...Registerer) *mux.Router {
	r := mux.NewRouter()
	r.Use(logMiddleware)
	r.Use(corsMiddleware)
	r.Use(preflightMiddleware)

	for _, c := range controllers {
		c.RegisterEndpoints(r)
	}

	return r
}
