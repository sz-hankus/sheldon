package router

import (
	"errors"
	"fmt"
	"net/http"
	"sheldonAPI/internal/domain/model"
	"sheldonAPI/internal/log"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

var (
	// TODO: make this configurable ofc XD
	jwtSecret = "hokus_pokus_simsalabim_jak_ktos_to_przeczyta_to_bedzie_dym"
)

// Errors
var (
	ErrWrongAuthHeader    = errors.New("Incorrect Authorization header. Use bearer tokens")
	ErrWrongSigningMethod = errors.New("Wrong jwt signing method")
	ErrInvalidClaims      = errors.New("Invalid claims")
)

func CreateJWT(u model.User) (string, error) {
	// https://auth0.com/docs/secure/tokens/json-web-tokens/json-web-token-claims
	// TODO: make signing method configurable
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub":  u.Email, // subject
		"id":   u.Id,
		"iat":  time.Now().Unix(),                // issued at time
		"exp":  time.Now().Add(time.Hour).Unix(), // expiry time, TODO: make this configurable
		"role": u.Role,
	})

	signed, err := token.SignedString([]byte(jwtSecret))
	if err != nil {
		log.Logger.Error("Could not sign token", "cause", err)
		return "", err
	}
	return signed, nil
}

func VerifyJWT(r *http.Request) (*jwt.Token, error) {
	h := r.Header.Get("Authorization")
	auth, found := strings.CutPrefix(h, "Bearer ")
	if !found {
		return &jwt.Token{}, ErrWrongAuthHeader
	}

	token, err := jwt.Parse(auth, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, ErrWrongSigningMethod
		}

		return []byte(jwtSecret), nil
	})

	if err != nil {
		log.Logger.Error("JWT not valid", "cause", err)
		return &jwt.Token{}, err
	}
	log.Logger.Debug(fmt.Sprintf("Verified token: %+v", token.Claims))
	return token, nil
}

func GetCreds(token *jwt.Token) (model.UserCreds, error) {
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		log.Logger.Error("Error when casting claims", "token", token)
		return model.UserCreds{}, ErrInvalidClaims
	}

	var creds model.UserCreds
	id, ok := claims["id"].(float64)
	if !ok {
		return model.UserCreds{}, errors.New("the claim 'id' could not be cast to a float64")
	}
	subject, ok := claims["sub"].(string)
	if !ok {
		return model.UserCreds{}, errors.New("the claim 'sub' (email) could not be cast to a uint64")
	}
	role, ok := claims["role"].(string)
	if !ok {
		return model.UserCreds{}, errors.New("the claim 'role' could not be cast to a string")
	}

	creds.Id = uint64(id)
	creds.Email = subject
	creds.Role = model.UserRole(role)

	return creds, nil
}
