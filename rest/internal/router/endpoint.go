package router

import (
	"net/http"

	"github.com/gorilla/mux"
)

type Endpoint struct {
	methods    []string
	path       string
	handleFunc func(http.ResponseWriter, *http.Request)
}

func (e *Endpoint) Register(r *mux.Router) {
	r.HandleFunc(e.path, e.handleFunc).Methods(e.methods...)
}

// Functional options pattern
func NewEndpoint(options ...func(*Endpoint)) *Endpoint {
	// default config - no roles required
	endpoint := Endpoint{}
	for _, o := range options {
		o(&endpoint)
	}
	return &endpoint
}

func WithMethods(methods ...string) func(*Endpoint) {
	return func(e *Endpoint) {
		e.methods = append(methods, http.MethodOptions) // always add OPTIONS to methods, for cors preflight requests
	}
}

func WithPath(path string) func(*Endpoint) {
	return func(e *Endpoint) {
		e.path = path
	}
}

func WithHandleFunc(handleFunc func(http.ResponseWriter, *http.Request)) func(*Endpoint) {
	return func(e *Endpoint) {
		e.handleFunc = handleFunc
	}
}
