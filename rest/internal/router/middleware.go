package router

import (
	"net/http"
	"sheldonAPI/internal/log"
	"time"
)

func logMiddleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		next.ServeHTTP(w, r)
		log.Logger.Debug("Handled endpoint", "method", r.Method, "path", r.URL.RequestURI(), "time(us)", time.Since(start).Microseconds())
	}
	return http.HandlerFunc(fn)
}

// to na razie sobie zostawmy na potrzeby frontendu, później będziemy myśleć XD
func corsMiddleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS, DELETE")
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}

// to na razie sobie zostawmy na potrzeby frontendu, później będziemy myśleć XD
func preflightMiddleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "OPTIONS" {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS, DELETE")
			w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization")
			return
		}
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}
