package usecase

import "errors"

var (
	ErrAccesDenied       = errors.New("Access denied")
	ErrStationNotFound   = errors.New("Station not found")
	ErrSavingByteContent = errors.New("Error when saving byte content")
)

var (
	ErrUserNotFound      = errors.New("User not found")
	ErrUserNotSaved      = errors.New("User could not be saved")
	ErrUserAlreadyExists = errors.New("The user already exists")
	ErrEmailDuplicate    = errors.New("Duplicated email address")
)

var (
	ErrReservationNotFound         = errors.New("Station not found")
	ErrReserverCantChooseLabMaster = errors.New("A reserver cannot chose a lab master")
	ErrReservationInThePast        = errors.New("Cannot make a reservation in the past")
	ErrReservationEndBeforeStart   = errors.New("Cannot make a reservation that ends before it starts")
	ErrLabMasterDoesntExist        = errors.New("Lab master with the specified id doesn't exits")
	ErrLabStationNotChosen         = errors.New("The Lab station was not chosen")
	ErrLabStationDoesntExist       = errors.New("The Lab station doesn't exist")
	ErrAdminHasToChooseReserver    = errors.New("An admin has to choose a reserver when making a reservation")
)
