package usecase

import (
	"io"
	"sheldonAPI/internal/domain/model"

	"github.com/google/uuid"
)

// Repo for storing byte content (e.g on File Systems (FSRepo))
type ByteRepo interface {
	Save(reader io.Reader) (id uuid.UUID, err error)
}

type FinderSaver[T any] interface {
	FindAll() ([]T, error)
	Find(id uint) (T, error)
	Save(newT T) (T, error)
}

type UserRepo interface {
	FinderSaver[model.User]
	FuzzyFind(query string) ([]model.User, error)
	Search(email string) ([]model.User, error)
	Update(id uint, new model.User) (model.User, error) // TODO: maybe remove this id parameter?
	Delete(id uint) (model.User, error)
}

type LabStationRepo interface {
	FinderSaver[model.LabStation]
	// FuzzyFind(query string) ([]model.LabStation, error)
}

type ReservationRepo interface {
	FinderSaver[model.Reservation]
	FindByLabStationId(id uint) ([]model.Reservation, error)
	Update(new model.Reservation) (model.Reservation, error)
	// FuzzyFind(query string) ([]model.Reservation, error)
}
