package usecase

import (
	"database/sql"
	"sheldonAPI/internal/domain/model"
	"sheldonAPI/internal/log"
	"time"
)

type ReservationUseCase struct {
	ReservationRepo ReservationRepo
	UserRepo        UserRepo
	LabStationRepo  LabStationRepo
}

func (uc *ReservationUseCase) GetReservations() ([]model.Reservation, error) {
	reservations, err := uc.ReservationRepo.FindAll()
	if err != nil {
		return []model.Reservation{}, err
	}
	return reservations, nil
}

func (uc *ReservationUseCase) GetReservation(id uint) (model.Reservation, error) {
	rsrv, err := uc.ReservationRepo.Find(id)
	if err != nil {
		switch err {
		case sql.ErrNoRows:
			return model.Reservation{}, ErrReservationNotFound
		default:
			return model.Reservation{}, err
		}
	}

	return rsrv, nil
}

func (uc *ReservationUseCase) GetReservationsForLabStation(labStationId uint) ([]model.Reservation, error) {
	reservations, err := uc.ReservationRepo.FindByLabStationId(labStationId)
	if err != nil {
		switch err {
		case sql.ErrNoRows:
			return []model.Reservation{}, ErrReservationNotFound
		default:
			return []model.Reservation{}, err
		}
	}

	return reservations, nil
}

func (uc *ReservationUseCase) userExists(id uint) bool {
	_, err := uc.UserRepo.Find(id)
	if err != nil {
		switch err {
		case sql.ErrNoRows:
			return false
		default:
			log.Logger.Error("Could not find user", "cause", err)
			return false
		}
	}
	return true
}

func (uc *ReservationUseCase) labStationExists(id uint) bool {
	_, err := uc.LabStationRepo.Find(id)
	if err != nil {
		switch err {
		case sql.ErrNoRows:
			return false
		default:
			log.Logger.Error("Could not find lab station", "cause", err)
			return false
		}
	}
	return true
}

func (uc *ReservationUseCase) MakeReservation(creds model.UserCreds, rsrv model.Reservation) (model.Reservation, error) {
	var newReservation model.Reservation

	if rsrv.StartTime.Before(time.Now()) {
		return model.Reservation{}, ErrReservationInThePast
	}
	if rsrv.EndTime.Before(rsrv.StartTime) {
		return model.Reservation{}, ErrReservationEndBeforeStart
	}
	newReservation.StartTime = rsrv.StartTime
	newReservation.EndTime = rsrv.EndTime

	if rsrv.LabStationId == nil {
		return model.Reservation{}, ErrLabStationNotChosen
	} else {
		if !uc.labStationExists(uint(*rsrv.LabStationId)) {
			return model.Reservation{}, ErrLabStationDoesntExist
		}
		newReservation.LabStationId = rsrv.LabStationId
	}

	if creds.RoleIn(model.Admin, model.SuperAdmin) {
		if rsrv.ReserverId == nil {
			return model.Reservation{}, ErrAdminHasToChooseReserver
		}
		if !uc.userExists(uint(*rsrv.ReserverId)) {
			return model.Reservation{}, ErrUserNotFound
		}
		newReservation.ReserverId = rsrv.ReserverId

		if rsrv.LabMasterId != nil {
			if !uc.userExists(uint(*rsrv.LabMasterId)) {
				return model.Reservation{}, ErrLabMasterDoesntExist
			}
			newReservation.LabMasterId = rsrv.LabMasterId
			newReservation.Status = model.PendingStage2
		}
	}

	if creds.Role == model.LabMaster {
		return model.Reservation{}, ErrAccesDenied
	}

	if creds.Role == model.Reserver {
		newReservation.ReserverId = &creds.Id
		if rsrv.LabMasterId != nil {
			return model.Reservation{}, ErrReserverCantChooseLabMaster
		}
		if rsrv.Status == model.PendingStage2 {
			newReservation.Status = model.PendingStage2
		} else {
			newReservation.Status = model.PendingStage1
		}
	}

	newReservation.CreationTime = time.Now()
	newReservation.ChatLog = ""
	created, err := uc.ReservationRepo.Save(newReservation)
	if err != nil {
		return model.Reservation{}, err
	}
	return created, nil
}

func (uc *ReservationUseCase) reservationExists(id uint) bool {
	_, err := uc.ReservationRepo.Find(id)
	if err != nil {
		switch err {
		case sql.ErrNoRows:
			return false
		default:
			log.Logger.Error("Could not find reservation", "cause", err)
			return false
		}
	}
	return true
}

func (uc *ReservationUseCase) AcceptReservation(creds model.UserCreds, id uint) (updated model.Reservation, err error) {
	if creds.Role == model.Reserver {
		return model.Reservation{}, ErrAccesDenied
	}
	reservation, err := uc.ReservationRepo.Find(id)
	if err != nil {
		switch err {
		case sql.ErrNoRows:
			return model.Reservation{}, ErrReservationNotFound
		default:
			log.Logger.Error("Could not find reservation", "cause", err)
			return model.Reservation{}, err
		}
	}

	if creds.RoleIn(model.Admin, model.SuperAdmin) {
		if reservation.Status == model.PendingStage1 {
			return model.Reservation{}, ErrAccesDenied
		}
		reservation.Status = model.Accepted
	}

	if creds.Role == model.LabMaster {
		if reservation.Status == model.PendingStage2 {
			return model.Reservation{}, ErrAccesDenied
		}
		reservation.LabMasterId = &creds.Id
		reservation.Status = model.PendingStage2
	}

	updated, err = uc.ReservationRepo.Update(reservation)
	if err != nil {
		return model.Reservation{}, err
	}

	return updated, nil
}
