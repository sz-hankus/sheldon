package usecase

import (
	"database/sql"
	"io"
	"sheldonAPI/internal/domain/model"
)

type LabStationUseCase struct {
	LabStationRepo LabStationRepo
	ImageRepo      ByteRepo
}

func (uc *LabStationUseCase) GetStations() ([]model.LabStation, error) {
	stations, err := uc.LabStationRepo.FindAll()
	if err != nil {
		return []model.LabStation{}, err
	}
	return stations, nil
}

func (uc *LabStationUseCase) GetStation(id uint) (model.LabStation, error) {
	station, err := uc.LabStationRepo.Find(id)
	if err != nil {
		switch err {
		case sql.ErrNoRows:
			return model.LabStation{}, ErrStationNotFound
		default:
			return model.LabStation{}, err
		}
	}

	return station, nil
}

func (uc *LabStationUseCase) CreateStation(requester model.UserCreds, station model.LabStation, pictures []io.Reader) (model.LabStation, error) {
	if !requester.RoleIn(model.Admin, model.SuperAdmin) {
		return model.LabStation{}, ErrAccesDenied
	}

	picIds := []string{}
	for _, picture := range pictures {
		id, err := uc.ImageRepo.Save(picture)
		if err != nil {
			// TODO: remove the images that are already in picIds
			return model.LabStation{}, ErrSavingByteContent
		}
		picIds = append(picIds, id.String())
	}
	station.ImageRefs = picIds

	created, err := uc.LabStationRepo.Save(station)
	if err != nil {
		return model.LabStation{}, err
	}
	return created, nil
}
