package usecase

import (
	"database/sql"
	"io"
	"sheldonAPI/internal/domain/model"
	"sheldonAPI/internal/log"
	"time"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

type UserUseCase struct {
	UserRepo UserRepo
	ByteRepo ByteRepo
}

func (uc *UserUseCase) GetUsers(creds model.UserCreds) ([]model.User, error) {
	if !creds.RoleIn(model.Admin, model.SuperAdmin) {
		return []model.User{}, ErrAccesDenied
	}

	users, err := uc.UserRepo.FindAll()
	if err != nil {
		return []model.User{}, err
	}
	return users, nil
}

func (uc *UserUseCase) GetUser(id uint) (model.User, error) {
	users, err := uc.UserRepo.Find(id)
	if err != nil {
		return model.User{}, err
	}
	return users, nil
}

func (uc *UserUseCase) FuzzyFindUser(query string) ([]model.User, error) {
	users, err := uc.UserRepo.FuzzyFind(query)
	if err != nil {
		if err == sql.ErrNoRows {
			return []model.User{}, nil
		}
		return []model.User{}, err
	}
	return users, nil
}

func (uc *UserUseCase) CreateUser(req model.UserForm, profilePic io.Reader) (model.User, error) {
	existing, err := uc.UserRepo.Search(req.Email)
	if err != nil {
		log.Logger.Error("Could not search for a user", "cause", err)
		return model.User{}, nil
	}
	if len(existing) != 0 {
		return model.User{}, ErrUserAlreadyExists
	}

	imageId := uuid.UUID{}
	if profilePic != nil {
		imageId, err = uc.ByteRepo.Save(profilePic)
		if err != nil {
			log.Logger.Error("Could not save the profile picture", "email", req.Email, "cause", err)
			return model.User{}, ErrSavingByteContent
		}
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(req.Password), 12) // TODO: make the cost configurable
	if err != nil {
		return model.User{}, err
	}

	strId := imageId.String()
	u := model.User{
		Email:          req.Email,
		HashedPassword: string(hash),
		Role:           req.Role,
		CreationTime:   time.Now(),
		ImageRef:       &strId,
	}

	created, err := uc.UserRepo.Save(u)
	if err != nil {
		log.Logger.Error("Could not save user to the database", "user", u, "cause", err)
		return model.User{}, ErrUserNotSaved
	}

	return created, nil
}

func (uc *UserUseCase) Login(email, password string) (model.User, bool, error) {
	users, err := uc.UserRepo.Search(email)
	if err != nil {
		return model.User{}, false, err
	}

	if len(users) == 0 {
		return model.User{}, false, ErrUserNotFound
	}

	if len(users) != 1 {
		log.Logger.Error("found users with duplicate email", "email", users[0].Email)
		return model.User{}, false, ErrEmailDuplicate
	}
	u := users[0]

	err = bcrypt.CompareHashAndPassword([]byte(u.HashedPassword), []byte(password))
	if err != nil {
		return u, false, nil
	}
	return u, true, nil
}

func (uc *UserUseCase) UpdateUser(creds model.UserCreds, id uint, new model.User) (model.User, error) {
	u, err := uc.UserRepo.Find(id)
	if err != nil {
		if err == sql.ErrNoRows {
			return model.User{}, ErrUserNotFound
		} else {
			return model.User{}, err
		}
	}

	if !creds.RoleIn(model.Admin, model.SuperAdmin) && creds.Email != u.Email {
		return model.User{}, ErrAccesDenied
	}

	u, err = uc.UserRepo.Update(id, new)
	if err != nil {
		return model.User{}, err
	}
	return u, nil
}

func (uc *UserUseCase) Delete(creds model.UserCreds, id uint) (model.User, error) {
	u, err := uc.UserRepo.Find(id)
	if err != nil {
		if err == sql.ErrNoRows {
			return model.User{}, ErrUserNotFound
		} else {
			return model.User{}, err
		}
	}

	if !creds.RoleIn(model.Admin, model.SuperAdmin) && creds.Email != u.Email {
		return model.User{}, ErrAccesDenied
	}

	u, err = uc.UserRepo.Delete(id)
	if err != nil {
		if err == sql.ErrNoRows {
			return model.User{}, ErrUserNotFound
		} else {
			return model.User{}, err
		}
	}
	return u, nil
}
