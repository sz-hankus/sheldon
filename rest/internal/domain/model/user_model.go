package model

import (
	"time"
)

type UserRole string

var (
	Reserver   UserRole = "reserver"
	LabMaster  UserRole = "lab_master"
	Admin      UserRole = "admin"
	SuperAdmin UserRole = "super_admin"
)

type User struct {
	Id             uint64    `json:"id" db:"user_id"`
	Email          string    `json:"email" db:"email"`
	HashedPassword string    `json:"-" db:"password"`
	Role           UserRole  `json:"role" db:"role"`
	CreationTime   time.Time `json:"creationTime" db:"created_on"`
	ImageRef       *string   `json:"imageRef" db:"image_ref"`
}

// What the user provides when they want to register
type UserForm struct {
	Email    string   `json:"email"`
	Password string   `json:"password"`
	Role     UserRole `json:"role"`
}

// User credentials used for veryfing whether a user can perform a specific action.
// Used in the useCases layer.
type UserCreds struct {
	Id    uint64   `json:"id"`
	Email string   `json:"email"`
	Role  UserRole `json:"role"`
}

func (uc *UserCreds) RoleIn(roles ...UserRole) bool {
	for _, r := range roles {
		if uc.Role == r {
			return true
		}
	}
	return false
}
