package model

import (
	"time"
)

type ReservationStatus string

var (
	PendingStage1 ReservationStatus = "pending_stage1"
	PendingStage2 ReservationStatus = "pending_stage2"
	Accepted      ReservationStatus = "accepted"
	Cancelled     ReservationStatus = "cancelled"
	Archived      ReservationStatus = "archived"
)

// Pointer fields used for values that can be NULL
type Reservation struct {
	Id           uint64            `json:"id" db:"reservation_id"`
	ReserverId   *uint64           `json:"reserverId" db:"reserver_id"`
	LabMasterId  *uint64           `json:"labMasterId" db:"lab_master_id"`
	LabStationId *uint64           `json:"labStationId" db:"lab_station_id"`
	Status       ReservationStatus `json:"status" db:"status"`
	CreationTime time.Time         `json:"creationTime" db:"created_on"`
	StartTime    time.Time         `json:"startTime" db:"start_time"`
	EndTime      time.Time         `json:"endTime" db:"end_time"`
	ChatLog      string            `json:"chatLog" db:"chat_log"`
}
