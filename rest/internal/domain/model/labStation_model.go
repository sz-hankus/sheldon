package model

import (
	"github.com/lib/pq"
)

type LabStation struct {
	Id          uint64         `json:"id" db:"labstation_id"`
	Name        string         `json:"name" db:"name"`
	Description string         `json:"description" db:"description"`
	ImageRefs   pq.StringArray `json:"image_refs" db:"image_refs"`
}
