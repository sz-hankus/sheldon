package log

import (
	"context"
	"log/slog"
	"os"
	"path"
)

// Implements slog.Handler
type LevelHandler struct {
	level   slog.Leveler
	handler slog.Handler
}

func NewLevelHandler(level slog.Leveler, handler slog.Handler) LevelHandler {
	if lh, ok := handler.(LevelHandler); ok {
		// Avoid nested LevelHandlers
		handler = lh.handler
	}
	return LevelHandler{level: level, handler: handler}
}

func (h LevelHandler) Enabled(_ context.Context, level slog.Level) bool {
	return level >= h.level.Level()
}

func (h LevelHandler) Handle(c context.Context, r slog.Record) error {
	return h.handler.Handle(c, r)
}

func (h LevelHandler) WithAttrs(attrs []slog.Attr) slog.Handler {
	return NewLevelHandler(h.level, h.handler.WithAttrs(attrs))
}

func (h LevelHandler) WithGroup(name string) slog.Handler {
	return NewLevelHandler(h.level, h.handler.WithGroup(name))
}

var (
	Logger *slog.Logger
)

func init() {
	baseHandler := slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
		AddSource: true,
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.SourceKey {
				s := a.Value.Any().(*slog.Source)
				s.File = path.Base(s.File)
			}
			return a
		},
	})
	handler := &LevelHandler{
		level:   slog.LevelDebug, // TODO: make level configurable
		handler: baseHandler,
	}
	Logger = slog.New(handler)
}
