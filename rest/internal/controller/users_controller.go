package controller

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"sheldonAPI/internal/domain/model"
	"sheldonAPI/internal/domain/usecase"
	"sheldonAPI/internal/log"
	"sheldonAPI/internal/router"
	"strconv"

	"github.com/gorilla/mux"
)

type UserHTTPController struct {
	Usecase usecase.UserUseCase
}

func (c *UserHTTPController) RegisterEndpoints(r *mux.Router) {
	sub := r.PathPrefix("/users").Subrouter()
	endpoints := []router.Endpoint{
		*router.NewEndpoint(
			router.WithMethods(http.MethodPost),
			router.WithPath(""),
			router.WithHandleFunc(c.RegisterUser),
		),
		*router.NewEndpoint(
			router.WithMethods(http.MethodPost),
			router.WithPath("/login"),
			router.WithHandleFunc(c.Login),
		),
		*router.NewEndpoint(
			router.WithMethods(http.MethodGet),
			router.WithPath("/{id}"),
			router.WithHandleFunc(c.GetUser),
		),
		*router.NewEndpoint(
			router.WithMethods(http.MethodGet),
			router.WithPath(""),
			router.WithHandleFunc(c.GetUsers),
		),
		*router.NewEndpoint(
			router.WithMethods(http.MethodPut),
			router.WithPath("/{id}"),
			router.WithHandleFunc(c.UpdateUser),
		),
		*router.NewEndpoint(
			router.WithMethods(http.MethodDelete),
			router.WithPath("/{id}"),
			router.WithHandleFunc(c.DeleteUser),
		),
	}

	log.Logger.Info("registering endpoints for users api...")
	for _, e := range endpoints {
		e.Register(sub)
	}
}

func (c *UserHTTPController) GetUsers(w http.ResponseWriter, r *http.Request) {
	token, err := router.VerifyJWT(r)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	creds, err := router.GetCreds(token)
	if err != nil {
		log.Logger.Error("error when creating creds from token", "cause", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	params := r.URL.Query()
	if email := params.Get("email"); email != "" {
		users, err := c.Usecase.FuzzyFindUser(email)
		if err != nil {
			log.Logger.Error("Could not fuzzy find user", "cause", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(users)
		if err != nil {
			log.Logger.Error("Could not encode to json", "cause", err)
			w.WriteHeader(http.StatusInternalServerError)
			// TODO: handle displaying errors in json or smth
			fmt.Fprint(w, err.Error())
			return
		}
		return
	}

	users, err := c.Usecase.GetUsers(creds)
	if err != nil {
		switch err {
		case usecase.ErrAccesDenied:
			w.WriteHeader(http.StatusForbidden)
			return
		default:
			w.WriteHeader(http.StatusInternalServerError)
			log.Logger.Error("Could not get users", "cause", err)
			fmt.Fprint(w, err.Error())
			return
		}
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(users)
	if err != nil {
		log.Logger.Error("Could not encode to json", "cause", err)
		w.WriteHeader(http.StatusInternalServerError)
		// TODO: handle displaying errors in json or smth
		fmt.Fprint(w, err.Error())
		return
	}
}

func (c *UserHTTPController) GetUser(w http.ResponseWriter, r *http.Request) {
	// Only verify jwt. The usecase doesnt require checking role/email.
	_, err := router.VerifyJWT(r)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, err.Error())
		return
	}

	u, err := c.Usecase.GetUser(uint(id))
	if err == sql.ErrNoRows {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprint(w, err.Error())
		return
	} else if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, err.Error())
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(u)
	if err != nil { // TODO: handle error somehow
		log.Logger.Error("Could not encode to json", "cause", err)
	}
}

func (c *UserHTTPController) RegisterUser(w http.ResponseWriter, r *http.Request) {
	err := r.ParseMultipartForm(32 << 20)
	if err != nil {
		log.Logger.Error("Could not parse multipart", "cause", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	creds := r.MultipartForm.Value["credentials"]
	if len(creds) == 0 {
		http.Error(w, "No creds in the multipart body", http.StatusBadRequest)
		return
	}
	var userParams model.UserForm
	err = json.Unmarshal([]byte(creds[0]), &userParams)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	profilePic, err := getFilesFromReq("profile-picture", r)
	if err != nil {
		http.Error(w, "Could not read multipart file with the name 'profile-picture'", http.StatusInternalServerError)
		return
	}

	u, err := c.Usecase.CreateUser(userParams, profilePic[0])
	if err != nil {
		switch err {
		case usecase.ErrUserAlreadyExists:
			http.Error(w, "", http.StatusConflict)
			return
		default:
			http.Error(w, "", http.StatusInternalServerError)
		}
		return
	}

	token, err := router.CreateJWT(u)
	if err != nil {
		log.Logger.Error("Could not create jwt", "user", u, "cause", err)
		http.Error(w, "", http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(map[string]interface{}{"jwt": token}) // TODO: maybe make this a type?
	if err != nil {
		log.Logger.Error("Could not encode jwt", "cause", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (c *UserHTTPController) Login(w http.ResponseWriter, r *http.Request) {
	var userParams model.UserForm
	// TODO: add json validation (check if it contains all the fields)
	err := json.NewDecoder(r.Body).Decode(&userParams)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	u, valid, err := c.Usecase.Login(userParams.Email, userParams.Password)
	if err != nil {
		if err == usecase.ErrUserNotFound {
			w.WriteHeader(http.StatusNotFound)
		} else {
			log.Logger.Error("Error when trying to find and verify user", "cause", err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	} else if !valid {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	token, err := router.CreateJWT(u)

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(map[string]interface{}{"jwt": token}) // TODO: maybe make this a type?
	if err != nil {
		log.Logger.Error("Could not encode jwt", "cause", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

}

func (c *UserHTTPController) UpdateUser(w http.ResponseWriter, r *http.Request) {
	token, err := router.VerifyJWT(r)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	creds, err := router.GetCreds(token)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var userParams model.User
	// TODO: add json validation (check if it contains all the fields)
	err = json.NewDecoder(r.Body).Decode(&userParams)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, err.Error())
		return
	}

	u, err := c.Usecase.UpdateUser(creds, uint(id), userParams)
	if err != nil {
		switch err {
		case usecase.ErrUserNotFound:
			w.WriteHeader(http.StatusNotFound)
			return
		case usecase.ErrAccesDenied:
			w.WriteHeader(http.StatusForbidden)
			return
		default:
			w.WriteHeader(http.StatusInternalServerError)
			log.Logger.Error("Could not update user", "user", u, "cause", err)
			fmt.Fprint(w, err.Error())
			return
		}
	}
	w.WriteHeader(http.StatusOK)
}

func (c *UserHTTPController) DeleteUser(w http.ResponseWriter, r *http.Request) {
	token, err := router.VerifyJWT(r)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	creds, err := router.GetCreds(token)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, err.Error())
		return
	}

	u, err := c.Usecase.Delete(creds, uint(id))
	if err != nil {
		switch err {
		case usecase.ErrUserNotFound:
			w.WriteHeader(http.StatusNotFound)
			return
		case usecase.ErrAccesDenied:
			w.WriteHeader(http.StatusForbidden)
			return
		default:
			w.WriteHeader(http.StatusInternalServerError)
			log.Logger.Error("Could not delete user", "user", u, "cause", err)
			fmt.Fprint(w, err.Error())
			return
		}
	}
	w.WriteHeader(http.StatusOK)
}
