package controller

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"sheldonAPI/internal/log"
)

func getFilesFromReq(name string, r *http.Request) ([]io.Reader, error) {
	files := r.MultipartForm.File[name]
	if len(files) == 0 {
		return nil, nil
	}

	readers := []io.Reader{}
	for _, file := range files {
		r, err := file.Open()
		if err != nil {
			return nil, fmt.Errorf("Could not read picture from multipart: %w", err)
		}
		defer r.Close()
		readers = append(readers, r)
	}

	return readers, nil
}

// TODO: use this everywhere
func sendJSON(w http.ResponseWriter, data any) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err := json.NewEncoder(w).Encode(data)
	if err != nil {
		log.Logger.Error("Could not encode data: ", "cause", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

}
