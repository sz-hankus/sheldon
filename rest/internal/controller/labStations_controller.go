package controller

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sheldonAPI/internal/domain/model"
	"sheldonAPI/internal/domain/usecase"
	"sheldonAPI/internal/log"
	"sheldonAPI/internal/router"
	"strconv"

	"github.com/gorilla/mux"
)

type LabStationHTTPController struct {
	Usecase usecase.LabStationUseCase
}

func (c *LabStationHTTPController) RegisterEndpoints(r *mux.Router) {
	sub := r.PathPrefix("/lab-stations").Subrouter()
	endpoints := []router.Endpoint{
		*router.NewEndpoint(
			router.WithMethods(http.MethodGet),
			router.WithPath(""),
			router.WithHandleFunc(c.GetLabStations),
		),
		*router.NewEndpoint(
			router.WithMethods(http.MethodGet),
			router.WithPath("/{id}"),
			router.WithHandleFunc(c.GetLabStation),
		),
		*router.NewEndpoint(
			router.WithMethods(http.MethodPost),
			router.WithPath(""),
			router.WithHandleFunc(c.CreateStation),
		),
	}

	log.Logger.Info("registering endpoints for labstations api...")
	for _, e := range endpoints {
		e.Register(sub)
	}
}

func (c *LabStationHTTPController) GetLabStations(w http.ResponseWriter, r *http.Request) {
	_, err := router.VerifyJWT(r)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	stations, err := c.Usecase.GetStations()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Logger.Error("Could not get stations", "cause", err)
		// TODO: handle displaying errors in json or smth
		fmt.Fprint(w, err.Error())
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(stations)
	if err != nil {
		log.Logger.Error("Could not encode to json", "cause", err)
		w.WriteHeader(http.StatusInternalServerError)
		// TODO: handle displaying errors in json or smth
		fmt.Fprint(w, err.Error())
		return
	}
}

func (c *LabStationHTTPController) GetLabStation(w http.ResponseWriter, r *http.Request) {
	_, err := router.VerifyJWT(r)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	id_str := vars["id"]
	id, err := strconv.ParseUint(id_str, 10, 64)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, err.Error())
		return
	}

	station, err := c.Usecase.GetStation(uint(id))
	if err != nil {
		switch err {
		case usecase.ErrStationNotFound:
			w.WriteHeader(http.StatusNotFound)
			return
		default:
			w.WriteHeader(http.StatusInternalServerError)
			log.Logger.Error("Could not get stations", "cause", err)
			fmt.Fprint(w, err.Error())
			return
		}
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(station)
	if err != nil {
		log.Logger.Error("Could not encode to json", "cause", err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, err.Error())
		return
	}
}

func (c *LabStationHTTPController) CreateStation(w http.ResponseWriter, r *http.Request) {
	token, err := router.VerifyJWT(r)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	creds, err := router.GetCreds(token)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err = r.ParseMultipartForm(32 << 20)
	if err != nil {
		log.Logger.Error("Could not parse multipart", "cause", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	station := r.MultipartForm.Value["lab-station"]
	if len(station) == 0 {
		http.Error(w, "No station in the multipart body", http.StatusBadRequest)
		return
	}

	var newStation model.LabStation
	err = json.Unmarshal([]byte(station[0]), &newStation)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	stationPictures, err := getFilesFromReq("station-picture", r)
	if err != nil {
		http.Error(w, "Could not read multipart files with the name 'station-picture'", http.StatusInternalServerError)
		return
	}
	log.Logger.Debug(fmt.Sprintf("Got %d pictures for labstation %q", len(stationPictures), newStation.Name))

	created, err := c.Usecase.CreateStation(creds, newStation, stationPictures)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(created)
	if err != nil {
		log.Logger.Error("Could not encode labstation", "cause", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
