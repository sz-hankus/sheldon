package controller

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sheldonAPI/internal/domain/model"
	"sheldonAPI/internal/domain/usecase"
	"sheldonAPI/internal/log"
	"sheldonAPI/internal/router"
	"strconv"

	"github.com/gorilla/mux"
)

type ReservationHTTPController struct {
	Usecase usecase.ReservationUseCase
}

func (c *ReservationHTTPController) RegisterEndpoints(r *mux.Router) {
	sub := r.PathPrefix("/reservations").Subrouter()
	endpoints := []router.Endpoint{
		*router.NewEndpoint(
			router.WithMethods(http.MethodGet),
			router.WithPath(""),
			router.WithHandleFunc(c.GetReservations),
		),
		*router.NewEndpoint(
			router.WithMethods(http.MethodGet),
			router.WithPath("/{id}"),
			router.WithHandleFunc(c.GetReservation),
		),
		*router.NewEndpoint(
			router.WithMethods(http.MethodPost),
			router.WithPath(""),
			router.WithHandleFunc(c.CreateReservation),
		),
		*router.NewEndpoint(
			router.WithMethods(http.MethodPost),
			router.WithPath("/{id}/accept"),
			router.WithHandleFunc(c.AcceptReservation),
		),
	}

	log.Logger.Info("registering endpoints for reservations api...")
	for _, e := range endpoints {
		e.Register(sub)
	}
}

func (c *ReservationHTTPController) GetReservations(w http.ResponseWriter, r *http.Request) {
	_, err := router.VerifyJWT(r)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	reservations := make([]model.Reservation, 0)

	params := r.URL.Query()
	if lsId_str := params.Get("labStation"); lsId_str != "" {
		lsId, err := strconv.ParseUint(lsId_str, 10, 64)
		if err != nil {
			log.Logger.Info("labStation id could not be parsed to uint: " + lsId_str)
			http.Error(w, "labStation is not a valid id", http.StatusBadRequest)
			return
		}
		reservations, err = c.Usecase.GetReservationsForLabStation(uint(lsId))
		if err != nil {
			log.Logger.Error("Could not get reservations", "cause", err)
			http.Error(w, "", http.StatusInternalServerError)
			return
		}
	} else {
		reservations, err = c.Usecase.GetReservations()
		if err != nil {
			log.Logger.Error("Could not get reservations", "cause", err)
			http.Error(w, "", http.StatusInternalServerError)
			return
		}
	}

	if len(reservations) == 0 {
		// dumb hack to make go serialize this array as [] instead of null
		reservations = make([]model.Reservation, 0)
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(reservations)
	if err != nil {
		log.Logger.Error("Could not encode to json", "cause", err)
		w.WriteHeader(http.StatusInternalServerError)
		// TODO: handle displaying errors in json or smth
		fmt.Fprint(w, err.Error())
		return
	}
}

func (c *ReservationHTTPController) GetReservation(w http.ResponseWriter, r *http.Request) {
	_, err := router.VerifyJWT(r)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	id_str := vars["id"]
	id, err := strconv.ParseUint(id_str, 10, 64)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, id_str+" is not a valid id")
		return
	}

	rsrv, err := c.Usecase.GetReservation(uint(id))
	if err != nil {
		switch err {
		case usecase.ErrReservationNotFound:
			w.WriteHeader(http.StatusNotFound)
			return
		default:
			w.WriteHeader(http.StatusInternalServerError)
			log.Logger.Error("Could not get reservation", "cause", err)
			fmt.Fprint(w, err.Error())
			return
		}
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(rsrv)
	if err != nil {
		log.Logger.Error("Could not encode to json", "cause", err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, err.Error())
		return
	}
}

func (c *ReservationHTTPController) CreateReservation(w http.ResponseWriter, r *http.Request) {
	token, err := router.VerifyJWT(r)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	creds, err := router.GetCreds(token)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var rsrv model.Reservation
	err = json.NewDecoder(r.Body).Decode(&rsrv)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Logger.Error("Could not decode from json", "cause", err)
		fmt.Fprint(w, err.Error())
		return
	}

	created, err := c.Usecase.MakeReservation(creds, rsrv)
	if err != nil {
		log.Logger.Error("Could not make reservation", "cause", err)
		switch err {
		case usecase.ErrAccesDenied:
			w.WriteHeader(http.StatusForbidden)
		case usecase.ErrReservationInThePast:
			w.WriteHeader(http.StatusBadRequest)
		case usecase.ErrReservationEndBeforeStart:
			w.WriteHeader(http.StatusBadRequest)
		case usecase.ErrAdminHasToChooseReserver:
			w.WriteHeader(http.StatusBadRequest)
		case usecase.ErrUserNotFound:
			w.WriteHeader(http.StatusBadRequest)
		default:
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(created)
	if err != nil {
		log.Logger.Error("Could not encode reservation", "cause", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (c *ReservationHTTPController) AcceptReservation(w http.ResponseWriter, r *http.Request) {
	token, err := router.VerifyJWT(r)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	creds, err := router.GetCreds(token)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	vars := mux.Vars(r)
	id_str := vars["id"]
	id, err := strconv.ParseUint(id_str, 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	updated, err := c.Usecase.AcceptReservation(creds, uint(id))
	if err != nil {
		log.Logger.Error("Could not accept reservation", "cause", err)
		switch err {
		case usecase.ErrAccesDenied:
			w.WriteHeader(http.StatusForbidden)
		case usecase.ErrReservationNotFound:
			w.WriteHeader(http.StatusNotFound)
		default:
			log.Logger.Error("Could not accept reservation", "cause", err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(updated)
	if err != nil {
		log.Logger.Error("Could not encode reservation", "cause", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
