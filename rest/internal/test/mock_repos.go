package test

import (
	"errors"
	"fmt"
	"io"
	"sheldonAPI/internal/domain/model"

	"github.com/google/uuid"
)

type InMemoryUserRepo struct {
	users []model.User
}

func NewInMemoryUserRepo(users ...model.User) *InMemoryUserRepo {
	repo := &InMemoryUserRepo{}
	for _, u := range users {
		repo.Save(u)
	}
	return repo
}

func (repo *InMemoryUserRepo) FindAll() ([]model.User, error) {
	return repo.users, nil
}

func (repo *InMemoryUserRepo) Find(id uint) (model.User, error) {
	for _, u := range repo.users {
		if uint(u.Id) == id {
			return u, nil
		}
	}
	return model.User{}, errors.New(fmt.Sprintf("User with id %d not found", id))
}

func (repo *InMemoryUserRepo) FuzzyFind(query string) ([]model.User, error) {
	panic("not implemented")
}

func (repo *InMemoryUserRepo) Search(email string) ([]model.User, error) {
	found := []model.User{}
	for _, u := range repo.users {
		if u.Email == email {
			found = append(found, u)
		}
	}
	return found, nil
}

func (repo *InMemoryUserRepo) Save(user model.User) (model.User, error) {
	user.Id = uint64(len(repo.users))
	repo.users = append(repo.users, user)
	return user, nil
}

func (repo *InMemoryUserRepo) Update(id uint, new model.User) (model.User, error) {
	for i := 0; i < len(repo.users); i++ {
		if repo.users[i].Id == new.Id {
			repo.users[i].Email = new.Email
			repo.users[i].Role = new.Role
			return repo.users[i], nil
		}
	}
	return model.User{}, errors.New(fmt.Sprintf("User with id %d not found", id))
}

func (repo *InMemoryUserRepo) Delete(id uint) (model.User, error) {
	found, err := repo.Find(id)
	if err != nil {
		return model.User{}, errors.New(fmt.Sprintf("User with id %d not found", id))
	}
	var ind int
	for i := 0; i < len(repo.users); i++ {
		if uint(repo.users[i].Id) == id {
			ind = i
		}
	}
	repo.users = append(repo.users[:ind], repo.users[ind+1:]...)
	return found, nil
}

type MockByteRepo struct{}

func (repo MockByteRepo) Save(reader io.Reader) (uuid.UUID, error) {
	return uuid.New(), nil
}
