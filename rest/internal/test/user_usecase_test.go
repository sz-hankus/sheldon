package test

import (
	"sheldonAPI/internal/domain/model"
	"sheldonAPI/internal/domain/usecase"
	"testing"
	"time"

	"golang.org/x/crypto/bcrypt"
)

func createUser(form model.UserForm) model.User {
	hash, err := bcrypt.GenerateFromPassword([]byte(form.Password), 12)
	if err != nil {
		panic(err)
	}
	return model.User{Email: form.Email, HashedPassword: string(hash), Role: form.Role, CreationTime: time.Now()}
}

func Test_AddingUsers(t *testing.T) {
	adminForm := model.UserForm{Email: "admin@test.com", Password: "123456", Role: model.Admin}

	uc := usecase.UserUseCase{
		UserRepo: &InMemoryUserRepo{},
		ByteRepo: MockByteRepo{},
	}

	u, err := uc.CreateUser(adminForm, nil)
	if err != nil {
		t.Errorf("Error when creating a user: %s\n", err)
	}
	t.Logf("Created user: %+v\n", u)

	users, err := uc.GetUsers(model.UserCreds{Email: "anything", Role: model.Admin})
	if err != nil {
		t.Errorf("Error when retrieving users: %s\n", err)
	}
	if len(users) != 1 {
		t.Errorf("Expected %d users, got %d\n", 1, len(users))
	}

	users, err = uc.GetUsers(model.UserCreds{Email: "anything", Role: model.Reserver})
	if err != usecase.ErrAccesDenied {
		t.Errorf("Access to GetUsers allowed for reserver")
	}
}

func Test_LoggingIn(t *testing.T) {
	adminForm := model.UserForm{Email: "admin@test.com", Password: "123456", Role: model.Admin}

	uc := usecase.UserUseCase{
		UserRepo: NewInMemoryUserRepo(createUser(adminForm)),
		ByteRepo: MockByteRepo{},
	}

	userLogged, loggedIn, err := uc.Login(adminForm.Email, adminForm.Password)
	if err != nil {
		t.Errorf("Error when trying to log in: %s", err)
	}
	if !loggedIn {
		t.Errorf("User could not log in, even though the password was right")
	}
	if userLogged.Email != adminForm.Email {
		t.Errorf("Tried to log in as %s, but got a reponse with %s", adminForm.Email, userLogged.Email)
	}

	userLogged, loggedIn, err = uc.Login(adminForm.Email, "Można, jak najbardziej. Jeszcze jak")
	if err != nil {
		t.Errorf("Error when trying to log in with incorrect password: %s", err)
	}
	if loggedIn {
		t.Errorf("User was able to log in, even though the password was incorrect")
	}

	nonExistentEmail := "nonexistent@doesnt.exist"
	userLogged, loggedIn, err = uc.Login(nonExistentEmail, "123456")
	if err != usecase.ErrUserNotFound {
		t.Errorf("The user %s doesn't exist, but it did not raise an error", nonExistentEmail)
	}
	if loggedIn {
		t.Errorf("The user %s doesn't exist, but was still able to log in", nonExistentEmail)
	}
}

func Test_UpdatingUsers(t *testing.T) {
	adminForm := model.UserForm{Email: "admin@test.com", Password: "123456", Role: model.Admin}
	reserverForm1 := model.UserForm{Email: "reserver1@test.com", Password: "123456", Role: model.Reserver}
	reserverForm2 := model.UserForm{Email: "reserver2@test.com", Password: "123456", Role: model.Reserver}

	uc := usecase.UserUseCase{
		UserRepo: NewInMemoryUserRepo(createUser(adminForm), createUser(reserverForm1), createUser(reserverForm2)),
		ByteRepo: MockByteRepo{},
	}

	users, err := uc.GetUsers(model.UserCreds{Email: adminForm.Email, Role: model.Admin})
	if err != nil {
		t.Errorf("Error when retrieving users: %s\n", err)
	}
	if len(users) != 3 {
		t.Errorf("Expected %d users to be created, got %d\n", 3, len(users))
	}

	// reserver1 tries updating reserver2
	_, err = uc.UpdateUser(model.UserCreds{Email: reserverForm1.Email, Role: reserverForm1.Role}, 2, model.User{Email: "newemail", Role: model.Admin})
	if err != usecase.ErrAccesDenied {
		t.Error("Reserver can update other reservers.")
	}

	// reserver2 tries updating themselves
	newEmail := "reserver2new@test.com"
	updated, err := uc.UpdateUser(model.UserCreds{Email: reserverForm2.Email, Role: model.Reserver}, 2, model.User{Email: newEmail})
	if err != nil {
		t.Errorf("User %s was not able do update themselves", reserverForm2.Email)
	}
	if err != nil && updated.Email != newEmail {
		t.Errorf("User %s updated themselves, but the returned user doesn't have the new email. Got: %s, expected: %s", reserverForm2.Email, updated.Email, newEmail)
	}
}

func Test_DeletingUsers(t *testing.T) {
	adminForm := model.UserForm{Email: "admin@test.com", Password: "123456", Role: model.Admin}
	reserverForm1 := model.UserForm{Email: "reserver1@test.com", Password: "123456", Role: model.Reserver}
	reserverForm2 := model.UserForm{Email: "reserver2@test.com", Password: "123456", Role: model.Reserver}

	uc := usecase.UserUseCase{
		UserRepo: NewInMemoryUserRepo(createUser(adminForm), createUser(reserverForm1), createUser(reserverForm2)),
		ByteRepo: MockByteRepo{},
	}

	users, err := uc.GetUsers(model.UserCreds{Email: adminForm.Email, Role: model.Admin})
	if err != nil {
		t.Errorf("Error when retrieving users: %s\n", err)
	}
	if len(users) != 3 {
		t.Errorf("Expected %d users to be created, got %d\n", 3, len(users))
	}

	// reserver1 tries deleting reserver2
	_, err = uc.Delete(model.UserCreds{Email: reserverForm1.Email, Role: model.Reserver}, 2)
	if err != usecase.ErrAccesDenied {
		t.Error("Reserver can delete other reservers. They should not have access to deleting other reservers")
	}

	// reserver2 tries deleting themselves
	deleted, err := uc.Delete(model.UserCreds{Email: reserverForm2.Email, Role: model.Reserver}, 2)
	if err != nil {
		t.Errorf("User %s was not able do delete themselves", reserverForm2.Email)
	}
	if err != nil && deleted.Email != reserverForm2.Email {
		t.Errorf("User %s delted themselves, but the returned user was different", reserverForm2.Email)
	}
}
