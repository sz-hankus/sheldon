BEGIN;

ALTER TABLE lab_stations
DROP COLUMN IF EXISTS image_ref; 

ALTER TABLE lab_stations
ADD COLUMN IF NOT EXISTS image_refs text[]; 

COMMIT;
