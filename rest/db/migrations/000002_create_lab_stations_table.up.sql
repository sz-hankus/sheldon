BEGIN;

CREATE TABLE IF NOT EXISTS lab_stations(
	labstation_id serial PRIMARY KEY,
	name VARCHAR (100) NOT NULL,
	description VARCHAR (1000) NOT NULL,
	image_ref VARCHAR (100) NOT NULL
);

COMMIT;
