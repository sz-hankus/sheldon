BEGIN;

do $$
BEGIN
	IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'reservation_status') THEN
	CREATE TYPE reservation_status AS ENUM (
		'pending_stage1',
		'pending_stage2',
		'accepted',
		'cancelled',
		'archived'
	);
	END IF;
END
$$;



CREATE TABLE IF NOT EXISTS reservations(
	reservation_id serial PRIMARY KEY,
	reserver_id integer references users(user_id),
	lab_master_id integer references users(user_id),
	lab_station_id integer references lab_stations(labstation_id),
	status reservation_status NOT NULL,
	created_on TIMESTAMP NOT NULL,
	start_time TIMESTAMP NOT NULL,
	end_time TIMESTAMP NOT NULL,
	chat_log TEXT NOT NULL
);

COMMIT;
