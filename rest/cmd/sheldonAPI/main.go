package main

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sheldonAPI/internal/config"
	"sheldonAPI/internal/controller"
	"sheldonAPI/internal/domain/usecase"
	"sheldonAPI/internal/log"
	"sheldonAPI/internal/repository"
	"sheldonAPI/internal/router"
	"syscall"
)

func shutdown(cause string, exitCode int) {
	log.Logger.Info("Shutting down...", "cause", cause)
	// free resources etc.
	repository.CloseDB()
	os.Exit(exitCode)
}

func handleSignal(received os.Signal) {
	log.Logger.Info("Received signal", "signal", received)
	switch received {
	case syscall.SIGINT:
		shutdown(fmt.Sprintf("received signal %s", received), 1)
	case syscall.SIGTERM:
		shutdown(fmt.Sprintf("received signal %s", received), 1)
	case syscall.SIGKILL:
		shutdown(fmt.Sprintf("received signal %s", received), 1)
	}
}

func listenForSignals() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	received := <-sigs
	handleSignal(received)
}

func main() {
	log.Logger.Info("Starting app...")
	go listenForSignals()

	conf := config.GetConfig()

	postgresDB := repository.GetDB()
	fsRepo := repository.FSRepo{BasePath: conf.Static.BasePath}

	userController := controller.UserHTTPController{
		Usecase: usecase.UserUseCase{
			UserRepo: &repository.UserRepo{DB: postgresDB},
			ByteRepo: &fsRepo,
		},
	}

	labStationController := controller.LabStationHTTPController{
		Usecase: usecase.LabStationUseCase{
			ImageRepo:      &fsRepo,
			LabStationRepo: &repository.LabStationRepo{DB: postgresDB},
		},
	}

	reservationController := controller.ReservationHTTPController{
		Usecase: usecase.ReservationUseCase{
			ReservationRepo: &repository.ReservationRepo{DB: postgresDB},
			UserRepo:        &repository.UserRepo{DB: postgresDB},
			LabStationRepo:  &repository.LabStationRepo{DB: postgresDB},
		},
	}

	r := router.NewRouter(&userController, &labStationController, &reservationController)

	addr := fmt.Sprintf(":%d", conf.Server.Port)
	log.Logger.Info(fmt.Sprintf("Starting http server on %s", addr))
	err := http.ListenAndServe(addr, r)
	if err != nil {
		log.Logger.Error("Server failed to start.", "cause", err)
	}
}
